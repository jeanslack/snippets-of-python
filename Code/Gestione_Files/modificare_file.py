
import os 
cwd = os.getcwd()

# se lo apri con mode='r+' (cioè in lettura e scrittura) alla fine verrà
# appeso il contenuto preservando il vecchio, per questo c'è bisogno di 
# seek(0) e truncate().
 

with open('%s/.videomass2/videomass2.conf' % cwd, 'r+') as vid:
    data = vid.readlines()
    for i in data:
        if '# Writer:' in i:
            row = data.index(i)
    vid.seek(0)
    data[row] = '##### si Writer ma gnocca\n'
    # usare il metodo file.writelines() in scrittura se devi
    # scrivere o appendere una lista
    #vid.write(''.join(data))
    vid.writelines(data)
    vid.truncate()
    
    
# Con mode='w' (scrittura) non lo puoi leggere con i metodi read() ed
# readlines() ma lo puoi solo scrivere e il file verrà sovrascritto
# completamente a meno che non utilizzi mode='a' (append). Quindi
# bisogna prima leggere mode='r' e iterare sul contenuto e poi aprire
# il file in scrittura.

with open('%s/.videomass2/videomass2.conf' % cwd, 'r') as vid:
    data = vid.readlines()
    for i in data:
        if '# Copyright: (c)' in i:
            row = data.index(i)

with open('%s/.videomass2/videomass2.conf' % cwd, 'w') as vid:
    data[row] = "Non c'è sesso senza amore\n"
    vid.write(''.join(data))


''' open files ARGUMENTS :
open(file, 
     mode='r', 
     buffering=-1, 
     encoding=None, 
     errors=None, 
     newline=None, 
     closefd=True, 
     opener=None)
'''
