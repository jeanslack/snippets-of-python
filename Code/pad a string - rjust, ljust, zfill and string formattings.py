#!/usr/bin/env python3

"""
Python `zfill`, `rjust`, `ljust`, `Python String Formatting` and 
`Python f-strings` to pad a string.

Use the Python zfill method to left-pad a string with zeroes & alternatives to 
the zfill method. These alternatives included the rjust method and Python 
string formatting.
"""

#-----------------------------------------------------
# rjust: aggiungere caratteri all'inizio di una stringa:
#-----------------------------------------------------
"""
Nota che `rjust` e `ljust` possono essere usati con qualsiasi stringa e non
necessariamente numeri. Il carattere di riempimento deve essere composto
esattamente di un carattere. Se la larghezza è più corta o della stessa 
lunghezza della nostra stringa originale, il metodo restituisce semplicemente 
una copia della stringa originale.

Definition and Usage

The rjust() method will right align the string, using a specified 
character (space is default) as the fill character.

Syntax:
    string.rjust(length, character)
    
Parameter Values:
    Parameter | Description
    ----------|------------
    length    | Required. The length of the returned string.
    character | Optional. A character to fill the missing space 
              | (to the left of the string). Default is " " (space).
              
The following example shows the usage of rjust() method.

"""
track = {'TRACK_NUM': '6'}
num = str(track['TRACK_NUM']).rjust(2, '0')

string = "this is string example....wow!!!"
print(string.rjust(50, '*'))

# Return a 20 characters long, right justified version of the word "banana":
txt = "banana"
x = txt.rjust(20)
print(x, "is my favorite fruit.") 

# Using the letter "O" as the padding character:
txt = "banana"
x = txt.rjust(20, "O")
print(x) 

#-----------------------------------------------------
# zfill: aggiungere degli zero all'inizio di una stringa:
#-----------------------------------------------------
"""
A differenza di rjust/ljust, zfill riempie l'inizio di una stringa solo con 
degli zeri. Come rjust/ljust, se la larghezza è più corta o della stessa 
lunghezza della nostra stringa originale, il metodo restituisce semplicemente 
una copia della stringa originale.

How to Use Python zfill to Pad a String in Python with Zeros

The Python zfill method is used to create a copy of a string with zeros padding 
the string to a particular width. 
"""
track = {'TRACK_NUM': '6'}
num = str(track['TRACK_NUM']).zfill(2)

# Using Python zfill to pad a string
a_string = 'datagy'
print(a_string.zfill(10))  # Returns: 0000datagy

#-----------------------------------------------------
# string formatting: aggiungere degli zero all'inizio di una stringa:
#-----------------------------------------------------

# Right Pad a String with Python String Formatting
# Pad a String with String Formatting
a_string = 'datagy'
print('{:0>10}'.format(a_string))  # Returns: 0000datagy

#-----------------------------------------------------
# f-string: aggiungere degli zero all'inizio di una stringa:
#-----------------------------------------------------

# Similarly, we can use Python f-strings to pad a string:
# Pad a String with F-Strings
a_string = 'datagy'
print(f'{a_string:0>10}')  # Returns: 0000datagy
"""
This approach is a bit more readable as it makes it immediately clear 
what string you’re hoping to pad.
"""


