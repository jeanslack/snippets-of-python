#!/usr/bin/env python3

"""
eval: valuta ed esegue una espressione. Una espressione è tutto ciò
      che può essere contenuto in una variabile
      
repr: quota una espressione. mostra la rappresentazione reale di un oggetto

exec: esegue un blocco di codice

compile: (funzione) Compila il sorente in un codice oggetto <code object..> 
         che può essere eseguito da exec() o eval(). 
         
         ESEMPIO: eval(compile("print('12 square root:',12**0.5)",'<string>','single'))
         
         Il codice sorgente potrebbe essere rappresentato da un modulo Python,
         da uno statement o una espressione. Prende 6 argomenti:
         
         compile(source, filename, mode, flags=0, dont_inherit=False, optimize=-1)
         
         dei quali i primi tre sono mandatori.
         
         L'argomento filename prende il nome del file sorgente da leggere,
         altrimenti può avere un nome di propria scelta.
         L'argomento mode deve essere 'exec' per compilare un modulo, 
         'single' per compilare un solo (interattivo) statement, o 'eval'
         per compilare un'espressione.
"""
         

code = '''
def prova():
    l = [] 
    for x in range(10):
        l.append(x)
    return l
print(prova())'''

