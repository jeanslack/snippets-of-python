#!/usr/bin/env python
# -*- coding: UTF-8 -*-
#
import os
import string
from xml.dom.minidom import parseString
array = [u'Da WAV a MP3', u'Converte files wav in mp3 a un bitrate di 128k', u'-ab 128', u'wav', u'mp3']


#<label name="TERZO" type="memo da videomass">
#<parameters>-12 --33</parameters>
#<filesupport>AVI APE FLAC</filesupport>
#<extension>avi</extension>
#</label>

#name = "prova scrittura"
#descr = "la descrizione è valida"
#command_join = "22 33 -cc compress"
#diction_command = {"VideoFormat": "avi"}


#dirconf = os.path.expanduser('~/.videomass/av_profile.xml')
#fileconf = open('%s' % (dirconf),'r').readlines()
#raw_list = string.join(fileconf)
#names = 'name="%s"' % (name)
#cod_names = names.encode("utf-8")

#del fileconf[len(fileconf)-1]
#open('%s' % (dirconf),'w').writelines(fileconf)
#fileconf = open('%s' % (dirconf),'a')

#model = """
    #<label name="%s" type="%s">
        #<parameters>%s</parameters>
        #<filesupport> </filesupport>
        #<extension>%s</extension>
    #</label>
#</presets>
#""" % (name, descr, command_join, diction_command["VideoFormat"])


##Coding = model.encode("utf-8")
#fileconf.writelines(model)
#fileconf.close()


def parser_list(arg): # arg è il nome del file xml
    """
    here, make parsing to .xml files in '~/.videomass' for reads from preset
    functionality program in videomass where this organization list is shown.

    """
    dirname = os.path.expanduser('~/')
    File = open('%s/%s.xml' %(dirname, arg),'r')
    datos = File.read()
    File.close()

    parser = parseString(datos) # fa il parsing del file xml ed esce:
                            # <xml.dom.minidom.Document instance at 0x8141eec>
    dati = dict()
    for presets in parser.getElementsByTagName("presets"):
        #for preset in presets.getElementsByTagName("label"):
        name = preset.getAttribute("name")
        types = preset.getAttribute("type")
        parameters = None
        filesupport = None
        extension = None

        for presets in preset.getElementsByTagName("parameters"):
            for parameters in presets.childNodes:
                params = parameters.data

        for presets in preset.getElementsByTagName("filesupport"):
            for filesupport in presets.childNodes:
                support = filesupport.data

        for presets in preset.getElementsByTagName("extension"):
            for extension in presets.childNodes:
                ext = extension.data

        dati[name] = { "type": types, "parametri": params,
                                "filesupport": support, "estensione": ext }
    #return dati[name]
    return dati


def delete_preset(array):
    """
    Funzione usata nel modulo arraymass.py per cancellare voci nel file
    av_profile.xml .

    dati è il dizionario iterabile passato dal modulo os_processing.parser_list
    (parsa il file xml) è composto da dizionari contenente altri dizionari
    nella forma {name:{extens:?, descript:?, command:?}}, etc)

    array è solo una chiave del diz messa in array e contiene name descr command
    support ext, cioè 5 elementi. Array viene creata in arraymass nelle funzione
    # def on_select()

    """

    dati = parser_list('av_profile')
    dirconf = os.path.expanduser('~/av_profile.xml')

    """
    Posso anche usare i dizionari al posto degli indici lista nella riga 335
    (ho deciso per gli indici lista in questa versione), es:


    #name_profile = array[0].encode("utf-8")
    #description = param["type"].encode("utf-8")
    #commands_ffmpeg = param["parametri"].encode("utf-8")
    #file_support = param["filesupport"].encode("utf-8")
    #file_extension = param["estensione"].encode("utf-8")

    """

    param = dati[array[0]] # da il dizionario completo

    name_profile = array[0].encode("utf-8")
    description = array[1].encode("utf-8")
    #commands_ffmpeg = array[2].encode("utf-8")
    #file_support = array[3].encode("utf-8")
    #file_extension = array[4].encode("utf-8")

    #fileconf = open('%s' % (dirconf),'r').readlines()
    datos = open('%s' % (dirconf),'r')
    fileconf = datos.readlines()
    datos.close()

    """
    È possibile usare anche questo sistema con il metodo .index() ma sembra
    causare qualche errore. Non trova title=fileconf.index a volte: (incerto)

    #title = fileconf.index('    <label name="%s" type="%s">\n' % (
                            #name_profile, description)
                            #) # metodo index da un intero come indice lista

    #del fileconf [title + 1] # label iniziale
    #del fileconf [title + 1] # command ffmpeg
    #del fileconf [title + 1] # file support
    #del fileconf [title + 1] # estensione
    #del fileconf [title + 1] # lo spazio
    #del fileconf [title] # per ultimo cancello il name e type

    title + 1 (oppure indice +1 -vedi sotto-) avanza sempre di uno rispetto a
    title, e sempre solo a quello. Questa soluzione dovrebbe impedire di
    eliminare identiche occorrenze tipo <filesupport> </filesupport> nella
    ricerca in fileconf

    """
    title = '    <label name="%s" type="%s">\n' % (name_profile, description)

    for indice, riga in enumerate(fileconf): # mi da l'indice (è come index)
        if title in riga:
            print enumerate(fileconf)
            print riga
            print indice
            for i in range(0,5): # ripete la stessa esecuzione per 4 volte
                del fileconf[indice + 1]

            if fileconf [indice + 1] == "\n":
                del fileconf [indice + 1] # lo spazio
            del fileconf [indice] # per ultimo cancello il name e type

    #### TODO: c'è sempre una linea in più quando cancelli tutti i presets
    # questo non influisce sulla stabilità ma solo sulla formattazione del
    # testo. L'occhio vuole la sua parte!

    open('%s' % (dirconf),'w').writelines(fileconf)

parser_list('presets.xml')



