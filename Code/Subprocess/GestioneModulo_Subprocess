#!/usr/bin/env python
# -*- coding: utf-8 -*-

# riferimento: http://robyp.x10host.com/subprocess.html
# con aggiunta delle mie personalizzazioni

import sys

print """
Questo file riporta una guida ad alcuni processi di python.
Benchè presenti delle istruzioni eseguibili, è stato concepito
per un uso consultabile da lettura sorgente e non può essere 
eseguito così come è !

Inoltre non rispetta la PEP8 per gli stessi motivi.
"""

sys.exit()

import subprocess

"""
---------------  GESTIONE modulo SUBPROCESS:--------------------
La gestione del modulo subprocess permette di eseguire comandi o programmi,
comunemente processi, su diverse piattaforme dove è installato il linguaggio
di programmazione Python (linux, macOSX, windows, bsd, etc).
Il modulo subprocess fornisce un'interfaccia consistente per creare e lavorare 
con processi addizionali. Offre una interfaccia a livello più alto rispetto ad 
altri moduli disponibili, ed è destinato a rimpiazzare funzioni tipo os.system(), 
os.spawn*(), os.popen*(), os.popen2*() e commands.*(). 


Il modulo subprocess definisce una classe: Popen e qualche funzione wrapper che 
usa quella classe. Il costruttore per Popen riceve parecchi parametri per 
facilitare l'impostazione del nuovo processo, e quindi comunicare con esso 
tramite le pipe. Ci si concentrerà su un codice di esempio qui; per una 
descrizione completa degli argomenti fare riferimento alla sezione 17.1.1. 
della documentazione della libreria:
https://docs.python.org/2/library/subprocess.html?highlight=subprocess#subprocess

L'API è grossomodo la stessa, ma l'implementazione sottostante è leggermente 
diversa tra Unix e Windows. Tutti gli esempi qui mostrati sono testati su 
Linux e MAC OX X. L'esperienza personale (in un sistema operativo diverso da Unix) 
potrebbe essere diversa.
"""

###################################################################
#  -ESEGUIRE COMANDI ESTERNI (call, check_call, check_output):
###################################################################
"""
Se tu vuoi eseguire un comando o un programma e vedere il suo codice di ritorno
(exit status e tutto ciò che lo concerne) è sufficiente usare i metodi call(), 
check_call() o check_output().
La differenza è che check_call e check_output sollevano l'eccezione 
CalledProcessError, rispetto a call(), se il codice di ritorno non è zero.
Leggendo la documentazione di queste funzioni, non è raccomandato usare 
stdout=PIPE or stderr=PIPE. Se questo è quello che non si vuole, si dovrebbe 
usare la classe Popen, sulla quale segue una spiegazione più in basso.
"""
print '#------------------------------call()------------------------------#'
"""
( 0.0 ) 
Per eseguire un comando esterno senza interagire con esso, si si usa la 
funzione call()
"""

# Semplice comando
subprocess.call(['ls', '-l'], shell=True)


"""
( 0.1 )
Quando shell è impostato a True, le variabili di shell nella stringa di comando 
sono espanse
"""
# Comando con espansione della shell
subprocess.call('ls -l $HOME', shell=True)

"""
( 0.2 )
call gestisce gli errori allo stesso modo della shell. Quando un errore avviene,
da la segnalazione della shell più lo stato di uscita (exit status int) esempio:
"""
subprocess.call('programma_non_esistente', shell=True)
# output:
# /bin/sh: programma_non_esistente: command not found
# exit status:
# 127


print '#----------------------------check_call()------------------------------#'
"""
( 0.3 )
Se si vuole invece gestire gli errori con le eccezioni tipiche di python, si usa
check_call()
"""
subprocess.check_call('programma_non_esistente', shell=True)
# output:
"""
/bin/sh: programma_non_esistente: command not found
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
  File "/usr/lib/python2.7/subprocess.py", line 542, in check_call
    raise CalledProcessError(retcode, cmd)
subprocess.CalledProcessError: Command 'programma_non_esistente' returned non-zero exit status 127

In questo modo è possibile controllare gli errori con le clausole try, except, etc
"""


print '#----------------------check_output()---IO STREAM-----------------------#'
"""
( 0.4 )
Quando si ha la necessità di gestire l'output di un comando per un uso tipico
di redirezione (file, lista, print, etc) si usa check_output()
"""
subprocess.check_output('ls -l', shell=True)
# con variabile:
proc = subprocess.check_output('ls -l', shell=True)
print proc

"""
( 0.5 )
check_output solleva le eccezioni allo stesso modo di check_call ma fornisce una
interfaccia per la gestione dell'output automaticamente stringato (byte string).
Lo stesso output, a differenza dei precedenti, può venire contenuto su una 
varibile per esempio ed essere sucessivamente stampato dopo che il processo 
finisce.
"""
# Per catturare anche lo standard error nel risultato, utilizzare:
# stderr = subprocess.STDOUT:
subprocess.check_output("ls non_existent_file; exit 0",stderr=subprocess.STDOUT,shell=True)

# questo pezzo di codice lo preferisco:
# se uso e.output mi ritorna vuoto (non so perchè, edit: perchè devi dirigere lo
                                #   stdout allo sterr implementado questa variabile
                                #   dopo il comando: 'stderr=subprocess.STDOUT')
# se uso e.cmd mi da il comando
# se uso e.returncode ritorna lo stato di uscita
lis = raw_input('Importa qua la musica : ')
comando = 'shntool conv -o wav %s' % lis
try:
  subprocess.check_output(comando, shell = True)
except subprocess.CalledProcessError as e:
  print "\033[31;1mERROR:\033[0m %s" %(e)
if e.returncode is not 0:
    print "questo è l'errore: %s e il suo stato di uscita: %s" % (e.cmd, e.returncode)


print '#---------------------considerazioni finali--------------------------#'
"""
( 0.6 )
I vari call(), check_call(), e check_output(), usano Popen al loro interno.
Quando un comando viene eseguito senza altri parametri opzionali, senza alcuna
pipe o espansioni di variabile, etc, semplicemente può venire composto senza
l'uso della lista e lo splitting di ogni opzione:
"""
subprocess.call('ls')
subprocess.check_call('free')
subprocess.check_output('time')

"""
( 0.7 )
Se un comando presenta opzioni, con il codice sopra verrebbero
sollevate delle ecezioni, esempio:
"""
subprocess.call('ls -l')
# OSError: [Errno 2] No such file or directory

"""
( 0.8 )
Per ovviare a ciò, il comando va sempre stringato, splittato su ogni opzione 
separata da virgole e racchiuso in una lista racchiusa su una tupla:
"""
subprocess.call(['ls', '-l'])

"""
( 0.9 )
Se un comando presenta anche delle pipe |, espansioni $HOME, o gli operatori di 
controllo && (&& Dal punto di vista pratico, viene eseguito il secondo comando 
solo se il primo ha terminato il suo compito con successo.), si otterrebbe un 
altro tipo di errore: 
"""
subprocess.call(['ls', '-l', '&&', 'free', '-m'])
# ls: impossibile accedere a &&: File o directory non esistente
# ls: impossibile accedere a free: File o directory non esistente
# 2

"""
( 1.0 )
allora si deve formare il comando direttamente senza l'uso dello splitting in 
una lista e specificare a subprocess l'uso del parametro shell=True
"""
subprocess.call('ls -l && free -m', shell=True)





#######################################################################
#  -LAVORARE CON LE PIPE: ( Popen, stderr, stdout, stdin, PIPE )
#######################################################################
"""
( 1.1 )
La differenza sostanziale tra le chiamate di cui sopra e Popen, è che Popen 
viene eseguito simultaneamente ad altro codice procedurale, ovvero, dal
momento che la classe Popen è istanziata, il comando parte avviato già da 
subito, a meno chè non si usi il parametro communicate(), o il parametro wait()
o poll(). E' anche possibile accedere al codice di ritorno con l'attributo 
returncode dopo aver usato wait() o poll().

"""
# semplice esecuzione di comando:
proc = subprocess.Popen('ls')

# comando con argomenti:
proc = subprocess.Popen(['ls', '-l'])

# comando + arg + operatore di controllo
proc = subprocess.Popen('ls -l && free -m', shell=True)

# leggere l'output con il metodo read():
f = subprocess.Popen('ls -l', stdout = subprocess.PIPE, shell = True).stdout
ele = f.read()
print ele

"""
( 1.2 )
Se dirigiamo l'output dello stdout o dello stderr attraverso le PIPE, usando 
delle variabili con communicate e andiamo a stampare il suo contenuto senza usare 
l'indice di lista [0] o [1], python ci stamperà il contenuto di una tupla.
Lo stdout è indicizzato a [0], mentre lo stderr è indicizzato a [1], a meno di
non combinare lo stderr allo STDOUT, nel qual caso verrà indicizzato a [0].
Se usiamoo due variabili separate , una per lo stderr e una per lo stdout,
l'indicizzazione non serve, poichè è già implicita con l'uso delle variabili (vedi
paragr 1.6)
"""

# Il metodo communicate() legge tutto l'output ed attende che il processo 
# figlio esca prima di ritornare.
# Lettura dall'output di una pipe:
proc = subprocess.Popen(['ls', '-l'], shell=True, stdout=subprocess.PIPE)
out = proc.communicate()
print out

"""
( 1.3 )
Indicando l'indice della tupla otteniamo la formattazione dell'output:
"""
out = proc.communicate()[0]
print out

"""
( 1.4 )
In altrnativa possiamo comporre il codice come unica riga e settare l'indice
a nostro piacimento:
"""
output = subprocess.Popen(["ls", "-l"], stderr=subprocess.PIPE, stdout=subprocess.PIPE, stdin=subprocess.PIPE).communicate()[0]

"""
( 1.5 )
dirigere lo stderr allo stdout. Lo stderr viene indicizzato a [0] in questo 
caso, l'output dello stderr e dello stdout vengono combinati assieme
"""
proc = subprocess.Popen(['ffmpeg'], shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
out = proc.communicate()[0]
print out

"""
( 1.6 )
Dirigere lo stdout e lo stderr in modo indipendente e controllare l'output
sia dello stderr e dello stdout come flussi separati:
"""
filename = '/home/gianluca/Video/1.avi'
cmnd = ['ffprobe', '-show_format', '-show_streams', '-v', 'error', '-pretty', filename]
proc = subprocess.Popen(cmnd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
output, error =  proc.communicate()

print output
if error:
	print error



#######################################################################
#  -DETERMINARE LA CORRETTA TOKENIZZAZIONE: ( shlex )
#######################################################################
"""
( 1.7 )
Nei casi in cui la formazione di un comando eseguibile risulti avere molti 
parametri ed opzioni (casi complessi), il modulo shlex ci fornisce una classe
con analizzatore lessicale (lexical analyzer object). L'argomento da inizializ-
zare deve essere un oggetto file da leggere con i metodi read() e readline(),
o semplicemente una stringa:
"""

import shlex
# command string
command = "ffprobe -show_format -show_streams -v error -pretty %s" % filename

args = shlex.split(command)
print args # see split command
# ['ffprobe', '-show_format', '-show_streams', '-v', 'error', '-pretty', '/MyHome/etc/etc.etc']

p = subprocess.Popen(args, stderr=subprocess.PIPE)



#######################################################################
#  -CONTROLLI SUL CODICE DI RITORNO: ( wait(), poll(), returncode )
#######################################################################
"""
( 1.8 )
A volte è necessario predisporre di una interfaccia per il controllo di uno
stato di uscita che segue l'applicazione di un processo. Nel paragrafo 1.1,
avevo detto che dal momento che la classe Popen è istanziata, il comando parte 
avviato già da subito, e se usiamo l'attributo returncode per vedere lo stato
di un processo, succede qualcosa di imprevisto:
"""

proc = subprocess.Popen('ls')

print proc.returncode
# None

"""
( 1.9 )
Il motivo è che returncode non è automaticamente settato quando un processo
risulta finito. E' necessario chiamare .wait() o .poll() per rendere effettivo
lo stato di uscita
"""

proc = subprocess.Popen('ls')
proc.wait()
# 0
print proc.returncode
# 0







