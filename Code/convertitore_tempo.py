#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from __future__ import division
import datetime
import time


def convert(sec):
    "con datetime"
    print(str(datetime.timedelta(sec)))
    
def convert2(sec):
    "con time"
    print(time.strftime("%H:%M:%S", time.gmtime(sec)))
    
def convert3(seconds):
    #seconds = 7600
    m, s = divmod(seconds, 60)
    h, m = divmod(m, 60)
    print("%d:%02d:%02d" % (h, m, s))

convert3(507)
convert(507)
convert2(507)

def calc(time_seq):
    """
    converti da secondi a formato ore minuti secondi
    """

    dur = time_seq.split()[3]
    cut = time_seq.split()[1]
    pos = dur.split(':')
    h,m,s = pos[0],pos[1],pos[2]
    dursum = (int(h)*3600+ int(m)*60+ int(s))
    print(dursum)
    
    if not '00:00:00' in cut:
        pos = cut.split(':')
        h,m,s = pos[0],pos[1],pos[2]
        cutsum = (int(h)*3600+ int(m)*60+ int(s))
        print(cutsum)
        
        totalsum = dursum-cutsum
    else:
        totalsum = dursum
            
    return totalsum

#somma = calc('-ss 00:12:00 -t 00:00:00')
somma = calc('-ss 01:12:45 -t 01:00:24')
print ('somm',somma)

#--------------------------------------------------------#
def calc_raw(time_seq):
    """
    converti da formato ore minuti secondi a somma in secondi
    ma senza flags -ss, -t
    """
    pos = time_seq.split(':')
    h,m,s = pos[0],pos[1],pos[2]
    totalsum = (int(h)*3600+ int(m)*60+ float(s))

            
    return totalsum

#somma = calc_raw('02:30:00')
#print (somma)
#----------------------------------------------------------#


def convertisecondi(sec):
    h = sec/3600
    h_1 = str(h)
    hour = h_1.split('.')[0]
    print(hour)
    
    m = sec/60
    print(m)
    m_1 = str(m)
    minute = m_1.split('.')[0]
    print(minute)
    #print (m,m_1,minute)
    s = m_1.split('.')[1]
    s_1 = int(s)
    print(s_1)
    
    #print s_1
    
    second = s_1*60
    #second = str(sec)[:1}
    
    print('%s:%s:%s' %(hour,minute[:2],str(second)[:2]))

#convertisecondi(507)
    
