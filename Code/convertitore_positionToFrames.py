#!/usr/bin/python3
# -*- coding: UTF-8 -*-

def pos_to_frames(pos) -> int:
    """
    Converts position into frames (MM:SS:FF)
    There are seventy five (75) frames (FF) to one second.

    :param pos:

    """
    minutes, seconds, frames = map(int, pos.split(':'))
    seconds = (minutes * 60) + seconds
    rate = 44100  # sample rate
    return (seconds * rate) + (frames * (rate // 75))

print(pos_to_frames('15:56:60'))














