#!/usr/bin/env python
# -*- coding: ISO-8859-15 -*-

# Form implementation generated from reading ui file 'testapp_ui.ui'
#
# Created: Tue Aug 30 21:20:08 2005
#      by: The PyQt User Interface Compiler (pyuic) 3.14.1
#
# WARNING! All changes made in this file will be lost!


from qt import *


class TestAppUI(QDialog):
    def __init__(self,parent = None,name = None,modal = 0,fl = 0):
        QDialog.__init__(self,parent,name,modal,fl)

        if not name:
            self.setName("TestAppUI")


        TestAppUILayout = QGridLayout(self,1,1,11,6,"TestAppUILayout")

        self.lineedit = QLineEdit(self,"lineedit")

        TestAppUILayout.addMultiCellWidget(self.lineedit,0,0,0,1)

        self.addbutton = QPushButton(self,"addbutton")

        TestAppUILayout.addWidget(self.addbutton,0,2)

        self.deletebutton = QPushButton(self,"deletebutton")

        TestAppUILayout.addMultiCellWidget(self.deletebutton,2,2,1,2)
        spacer1 = QSpacerItem(161,31,QSizePolicy.Expanding,QSizePolicy.Minimum)
        TestAppUILayout.addItem(spacer1,2,0)

        self.listview = QListView(self,"listview")
        self.listview.addColumn(self.__tr("Our items"))

        TestAppUILayout.addMultiCellWidget(self.listview,1,1,0,2)

        self.languageChange()

        self.resize(QSize(245,238).expandedTo(self.minimumSizeHint()))
        self.clearWState(Qt.WState_Polished)


    def languageChange(self):
        self.setCaption(self.__tr("qt tutorial testapp"))
        self.addbutton.setText(self.__tr("Add"))
        self.deletebutton.setText(self.__tr("Delete"))
        self.listview.header().setLabel(0,self.__tr("Our items"))


    def __tr(self,s,c = None):
        return qApp.translate("TestAppUI",s,c)
