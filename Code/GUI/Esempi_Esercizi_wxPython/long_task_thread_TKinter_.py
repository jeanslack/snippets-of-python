import time
import tkinter as tk
from threading import Thread
from pubsub import pub

#lock = threading.Lock()


class MainApplication(tk.Frame):
    def __init__(self, parent, *args, **kwargs):
        tk.Frame.__init__(self, parent, *args, **kwargs)
        self.parent = parent
        self.label = tk.Label(root, text="Temperature / Humidity")
        self.label.pack(side="top", fill="both", expand=True)


        pub.subscribe(self.listener, 'receiver_1')

        WorkerThread()

    def listener(self, txt):
        #with lock:
        #    """do your plot drawing things here"""
        self.label.configure(text=txt)


class WorkerThread(Thread):
    def __init__(self):
        #super(WorkerThread, self).__init__()
        Thread.__init__(self)
        self.daemon = True  # do not keep thread after app exit
        self._stop = False

        self.start()  # start the thread

    def run(self):
        """calculate your plot data here"""
        for i in range(20):
            if self._stop:
                break
            time.sleep(0.5)
            pub.sendMessage('receiver_1', txt=str(i))


if __name__ == "__main__":
    root = tk.Tk()
    root.wm_geometry("320x240+100+100")

    main = MainApplication(root)
    main.pack(side="top", fill="both", expand=True)

    #pub.subscribe(main.listener, 'receiver_1')

    #wt = WorkerThread()
    #wt.start()

    root.mainloop()
