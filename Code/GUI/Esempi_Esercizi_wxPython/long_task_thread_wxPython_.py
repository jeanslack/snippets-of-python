# -*- coding: UTF-8 -*-
# Name: long_task_thread.py
# Porpose: a combined example of GUI and thread (multi-platform)
# Compatibility: Python3
# Author: Gianluca Pernigotto <jeanlucperni@gmail.com>
# license: GPL3
# Rev: March.29.2021 *PEP8 compatible*
#########################################################

import wx
import subprocess
import sys
import platform
if not platform.system() == 'Windows':
    import shlex
import itertools
import os
from threading import Thread
import time
from pubsub import pub


def logWrite(cmd, sterr, logname, logdir):
    """
    writes the log to the long_task_thread.log file.
    Includes commands and status error (if any)
    during the `LongTask` thread

    """
    if sterr:
        apnd = "...%s\n\n" % (sterr)
    else:
        apnd = "%s\n\n" % (cmd)

    with open(os.path.join(logdir, logname), "a", encoding='utf8') as log:
        log.write(apnd)


class LongTask(Thread):
    """
    The purpose of this thread is not to block a GUI; convert one
    or more video files to mp3 format, write a long_task_thread.log
    file in the same location as this file; redirect loop count,
    stdout/sdterr and possible exceptions to the GUI.

    """
    this = os.path.realpath(os.path.abspath(__file__))  # this file
    path = os.path.dirname(this)
    # CONSTANTS
    LOGDIR = os.path.join(path)
    FFMPEG_URL = '/usr/bin/ffmpeg'
    FFMPEG_LOGLEV = '-loglevel info -stats -hide_banner -nostdin'
    FF_THREADS = '-threads 4'
    SUFFIX = ''  # you can add optional suffix to output file name
    NOT_EXIST_MSG = ("Is 'ffmpeg' installed on your system?")
    # ---------------------------------------------------------------

    def __init__(self, filenames):
        """.
        attributes defined here:

        self.stop_work_thread: if True, stop current thread
        self.filelist: list object
        self.count: Initial count of loops
        self.countmax: length of self.filelist
        self.logname: name of the log file

        """
        self.stop_work_thread = False  # process terminate
        self.filelist = filenames
        self.count = 0
        self.countmax = len(filenames)
        self.logname = 'long_task_thread.log'

        Thread.__init__(self)
        """initialize"""

        self.start()  # start the thread

    def run(self):
        """
        uses Popen to convert any video to mp3 via the
        FFmpeg command through which you need to read the
        stdout/stderr in real time.

        """
        for files in self.filelist:

            basename = os.path.basename(files)
            filename = os.path.splitext(basename)[0]  # fn without ext
            outdir = os.path.dirname(files)

            cmd = ('"%s" %s -i "%s" -vn -c:a libmp3lame %s '
                   '-y "%s/%s%s.mp3"' % (LongTask.FFMPEG_URL,
                                         LongTask.FFMPEG_LOGLEV,
                                         files,
                                         LongTask.FF_THREADS,
                                         outdir,
                                         filename,
                                         LongTask.SUFFIX,
                                         ))
            self.count += 1
            count = 'File %s/%s' % (self.count, self.countmax,)
            com = "%s\n%s" % (count, cmd)
            wx.CallAfter(pub.sendMessage,
                         "COUNT_EVT",
                         count=count,
                         fname=files,
                         end='',
                         )
            logWrite(com,
                     '',
                     self.logname,
                     LongTask.LOGDIR,
                     )  # write n/n + command only

            if not platform.system() == 'Windows':
                cmd = shlex.split(cmd)
                info = None
            else:  # Hide subprocess window on MS Windows
                info = subprocess.STARTUPINFO()
                info.dwFlags |= subprocess.STARTF_USESHOWWINDOW
            try:
                with subprocess.Popen(cmd,
                                      stderr=subprocess.PIPE,
                                      bufsize=1,
                                      universal_newlines=True,
                                      startupinfo=info,) as p:
                    for line in p.stderr:
                        wx.CallAfter(pub.sendMessage,
                                     "UPDATE_EVT",
                                     output=line,
                                     status=0,
                                     )
                        if self.stop_work_thread:
                            p.terminate()
                            break  # break second 'for' loop

                    if p.wait():  # error
                        wx.CallAfter(pub.sendMessage,
                                     "UPDATE_EVT",
                                     output=line,
                                     status=p.wait(),
                                     )
                        logWrite('',
                                 "Exit status: %s" % p.wait(),
                                 self.logname,
                                 LongTask.LOGDIR,
                                 )  # append exit status error
                    else:  # ok
                        wx.CallAfter(pub.sendMessage,
                                     "COUNT_EVT",
                                     count='',
                                     fname='',
                                     end='ok'
                                     )
            except (OSError, FileNotFoundError) as err:
                e = "%s\n  %s" % (err, LongTask.NOT_EXIST_MSG)
                wx.CallAfter(pub.sendMessage,
                             "COUNT_EVT",
                             count=e,
                             fname=files,
                             end='error',
                             )
                logWrite('',
                         "%s" % e,
                         self.logname,
                         LongTask.LOGDIR,
                         )  # append exit status error
                break

            if self.stop_work_thread:
                p.terminate()
                break  # break second 'for' loop

        time.sleep(.5)
        wx.CallAfter(pub.sendMessage, "END_EVT")
    # --------------------------------------------------------------------#

    def stop(self):
        """
        Sets the stop work thread to terminate the process
        """
        self.stop_work_thread = True


class MyGUI(wx.Frame):
    """
    Shows a frame containing a read-only text control,
    a stop button and a close button to close the application.

    """
    def __init__(self):
        """
        pub.subscribe below creates receivers for thread
        communication with the GUI:

        UPDATE_EVT, update the stdout/stderr
        COUNT_EVT, counts each file
        END_EVT, reports that the thread has ended

        """
        filenames = sys.argv[1:]  # filenames list
        self.ABORT = False  # if True set to abort current process
        self.ERROR = False  # if True, all the tasks was failed
        self.result = None  # result of the final process
        self.PARENT_THREAD = None

        wx.Frame.__init__(self, None, wx.ID_ANY,
                          "My form", size=(600, 500))

        panel = wx.Panel(self, wx.ID_ANY)
        mainSizer = wx.BoxSizer(wx.VERTICAL)

        self.OutText = wx.TextCtrl(panel, wx.ID_ANY, "",
                                   style=wx.TE_MULTILINE |
                                   wx.TE_READONLY |
                                   wx.TE_RICH2
                                   )
        mainSizer.Add(self.OutText, 1, wx.EXPAND | wx.ALL, 5)
        horsizer = wx.BoxSizer(wx.HORIZONTAL)
        mainSizer.Add(horsizer, 0, flag=wx.ALIGN_RIGHT | wx.RIGHT, border=0)
        self.button_stop = wx.Button(panel, wx.ID_STOP, ("Abort"))
        self.button_stop.Disable()
        horsizer.Add(self.button_stop, 0, wx.EXPAND | wx.ALL, 5)
        self.button_close = wx.Button(panel, wx.ID_CLOSE, "")
        horsizer.Add(self.button_close, 0, wx.EXPAND | wx.ALL, 5)
        panel.SetSizer(mainSizer)

        self.Bind(wx.EVT_BUTTON, self.on_stop, self.button_stop)
        self.Bind(wx.EVT_BUTTON, self.on_close, self.button_close)

        # receivers
        pub.subscribe(self.update_display, "UPDATE_EVT")
        pub.subscribe(self.update_count, "COUNT_EVT")
        pub.subscribe(self.end_proc, "END_EVT")

        if not filenames:
            self.OutText.AppendText('\nplease, provide one or more '
                                    'video clip as arguments\n')
        else:
            self.button_stop.Enable()
            self.PARENT_THREAD = LongTask(filenames)  # instance to the thread
    # ----------------------------------------------------------------------

    def update_display(self, output, status):
        """
        Receive messages from thread by wxCallafter
        and pubsub UPDATE_EVT

        WARNING: to avoid unexpected behaviors, it is strongly
        discouraged to use print here. If you want to print some
        terminal output, do it from the thread.

        """
        if not status == 0:  # error, exit status of the p.wait
            self.OutText.AppendText('\nFAILED !\n')
            self.result = 'failed'
            return  # must be return here

        if 'time=' in output:  # ...in processing
            self.OutText.AppendText(output)

        else:
            if [x for x in ('info', 'Info') if x in output]:
                self.OutText.AppendText('%s' % output)

            elif [x for x in ('Failed', 'failed', 'Error', 'error')
                    if x in output]:
                self.OutText.AppendText('%s' % output)

            elif [x for x in ('warning', 'Warning') if x in output]:
                self.OutText.AppendText('%s' % output)

            else:
                self.OutText.AppendText('%s' % output)
    # ----------------------------------------------------------------------

    def update_count(self, count, fname, end):
        """
        Receive messages from thread by wxCallafter
        and pubsub COUNT_EVT

        WARNING: to avoid unexpected behaviors, it is strongly
        discouraged to use print here. If you want to print some
        terminal output, do it from the thread.

        """
        if end == 'ok':
            self.OutText.AppendText('\nDone !\n')
            return

        if end == 'error':
            self.OutText.AppendText('\n%s\n' % (count))
            self.ERROR = True
        else:
            self.OutText.AppendText('\n%s : "%s"\n' % (count, fname))
    # ----------------------------------------------------------------------

    def end_proc(self):
        """
        Receive messages from thread by wxCallafter
        and pubsub END_EVT

        WARNING: to avoid unexpected behaviors, it is strongly
        discouraged to use print here. If you want to print some
        terminal output, do it from the thread.

        """
        if self.ERROR is True:
            self.OutText.AppendText('\nTASK Failed\n')

        elif self.ABORT is True:
            self.OutText.AppendText('\nTASK Abort\n')

        else:
            if not self.result:
                self.OutText.AppendText('\nCOMPLETED\n')
            else:
                self.OutText.AppendText('\nNOT COMPLETED\n')

        self.button_stop.Disable()
        self.PARENT_THREAD = None
    # ----------------------------------------------------------------------

    def on_stop(self, event):
        """
        The user change idea and was stop process

        WARNING: to avoid unexpected behaviors, it is strongly
        discouraged to use print here. If you want to print some
        terminal output, do it from the thread.
        """
        self.PARENT_THREAD.stop()
        self.PARENT_THREAD.join()
        self.ABORT = True

        event.Skip()
    # ----------------------------------------------------------------------

    def on_close(self, event):
        """
        Close app

        """
        if self.PARENT_THREAD is not None:
            wx.MessageBox('There are still processes running.. if you want '
                          'to stop them, use the "Abort" button first.',
                          'WARNING', wx.ICON_WARNING, self)
            return

        else:
            self.Destroy()


# Run the program
if __name__ == "__main__":
    app = wx.App(False)
    frame = MyGUI()
    frame.Show()
    app.MainLoop()
