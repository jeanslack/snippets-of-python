#!/usr/bin/env python
# -*- coding: UTF-8 -*-
#
# Fri Aug 22 10:17:43 2014
#
# HOW TO CREATE A SIMPLE LISTCTRL

import wx


class MyFrame(wx.Frame):
    """
    Il widget list_control dispone principalmente di quattro modalità:
    icon, small icon, list, and report. In questo esempio utilizzo la
    modalità report, che è abbastanza comune fra gli sviluppatori.
    Ciò permette di aggiungere delle linee ogni volta che l'evento viene
    scatenato dalla pressione del bottone add line.
    """
    def __init__(self, *args, **kwds):

        kwds["style"] = wx.DEFAULT_FRAME_STYLE
        wx.Frame.__init__(self, *args, **kwds)
        self.panel_1 = wx.Panel(self, wx.ID_ANY)
        self.list_ctrl = wx.ListCtrl(self.panel_1, wx.ID_ANY, style=wx.LC_REPORT | wx.SUNKEN_BORDER)
        self.button = wx.Button(self.panel_1, wx.ID_ANY, ("Add Line"))

        #__set_properties
        self.SetTitle("Demo aggiungi linee di memorizzazione")
        self.list_ctrl.SetMinSize((700, 400))

        # qui è possibile aggiungere più colonne nella ListCtrl
        # chiamando il metodo InsertColumn()
        self.list_ctrl.InsertColumn(0, 'Row Number', width=230)
        self.list_ctrl.InsertColumn(1, 'Date', width=230)
        self.list_ctrl.InsertColumn(2, 'Location', width=350)

        #__do_layout
        sizer_1 = wx.BoxSizer(wx.VERTICAL)
        grid_sizer_1 = wx.FlexGridSizer(2, 1, 0, 0)
        grid_sizer_1.Add(self.list_ctrl, 1, wx.ALL | wx.EXPAND, 15)
        grid_sizer_1.Add(self.button, 0, wx.ALL, 15)
        self.panel_1.SetSizer(grid_sizer_1)
        grid_sizer_1.AddGrowableRow(0)
        grid_sizer_1.AddGrowableRow(1)
        grid_sizer_1.AddGrowableCol(0)
        sizer_1.Add(self.panel_1, 1, wx.EXPAND, 0)
        self.SetSizer(sizer_1)
        sizer_1.Fit(self)
        self.Layout()


        self.Bind(wx.EVT_BUTTON, self.add_line, self.button)

    def add_line(self, event):
        """
        Quando si chiama il metodo InsertStringItem si dà l'indice di riga
        corretta e una stringa. Si utilizza il metodo SetStringItem per
        impostare i dati per le altre colonne della riga. Si noti che il metodo
        SetStringItem richiede tre parametri: l'indice di riga, l'indice di
        colonna e una stringa. Infine, incrementiamo l'indice di riga in modo
        da non sovrascrivere nulla.
        """
        self.index = 0

        line = "Line %s" % self.index
        self.list_ctrl.InsertItem(self.index, line)
        self.list_ctrl.SetItem(self.index, 1, "22/08/2014")
        self.list_ctrl.SetItem(self.index, 2, "ITALIA")
        self.index += 1

        #event.Skip()


if __name__ == "__main__":

    app = wx.App(0)
    frame_1 = MyFrame(None, wx.ID_ANY, "")
    app.SetTopWindow(frame_1)
    frame_1.Show()
    app.MainLoop()
