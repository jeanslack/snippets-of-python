#!/usr/bin/env python
# -*- coding: UTF-8 -*-
#
# Thu Aug 21 19:16:20 2014
#

import wx


class MyFrame(wx.Frame):
    def __init__(self, *args, **kwds):
        """
        Questo esempio può tornare utile nel caso di iterazioni su chiavi e
        valori dei dizionari o con altre classi/Moduli che lavorano sul ritorno
        dati usando i dizionari (xml.dom.minidom per il parsing xml,
        memorizzazione dati con shelve, etc). Stampa le chiave e i valori dei
        dizionari e li separa nella list control. E' possibile anche usare le
        liste al posto dei dizionari e iterare su di esse in maniera analoga.
        Qui, la selezione di una riga comporta la scatenazione di un evento.
        """

        kwds["style"] = wx.DEFAULT_FRAME_STYLE
        wx.Frame.__init__(self, *args, **kwds)
        self.panel_1 = wx.Panel(self, wx.ID_ANY)
        self.list_ctrl = wx.ListCtrl(self.panel_1, wx.ID_ANY, style=wx.LC_REPORT | wx.SUNKEN_BORDER)

        #set_properties
        self.SetTitle("Demo seleziona catalogo in listctrl")
        self.list_ctrl.SetMinSize((700, 400))

        #__do_layout
        sizer_1 = wx.BoxSizer(wx.VERTICAL)
        grid_sizer_1 = wx.GridSizer(1, 1, 0, 0)
        grid_sizer_1.Add(self.list_ctrl, 1, wx.ALL | wx.EXPAND, 15)
        self.panel_1.SetSizer(grid_sizer_1)
        sizer_1.Add(self.panel_1, 1, wx.EXPAND, 0)
        self.SetSizer(sizer_1)
        sizer_1.Fit(self)
        self.Layout()

        self.Bind(wx.EVT_LIST_ITEM_SELECTED, self.on_select, self.list_ctrl)


        self.set_listctrl()

    def set_listctrl(self):
        """
        setting list_ctrl method
        """

        diz_a = {'Bash':'Bourn Again shell',
                'Python':'Il serpente dal linguaggio interpretato',
                'C++':'Linguaggio compilato',
                'O.O.P.':'Object oriented programming'}

        diz_b = {'Rudiments':'Technique for stick controll',
                        'Independence':'Four way coordination',
                        'Stick Patterns':'Play over the bar line',
                        'Broken time':'Technique used in the modern jazz'}

        diz_1 = {'PROGRAMMING': diz_a,'DRUMMING' : diz_b}


        self.list_ctrl.InsertColumn(0, 'Chiave del Dizionario', width=230)
        self.list_ctrl.InsertColumn(1, 'Valore del Dizionario', width=350)

        PROGRAMMING_keys = diz_1['PROGRAMMING'].keys()
        PROGRAMMING_val = diz_1['PROGRAMMING'].values()
        index_1 = 0
        index_2 = 0


        for name in PROGRAMMING_keys:
            self.list_ctrl.InsertItem(index_1, name)
            self.list_ctrl.SetItem(index_1, 0, name)
            index_1 += 1

        for descript in PROGRAMMING_val:
            self.list_ctrl.SetItem(index_2, 1, descript)
            index_2 += 1

        DRUMMING_keys = diz_1['DRUMMING'].keys()
        DRUMMING_val = diz_1['DRUMMING'].values()
        index_1 = 0
        index_2 = 0

        for name in DRUMMING_keys:
            self.list_ctrl.InsertItem(index_1, name)
            self.list_ctrl.SetItem(index_1, 0, name)
            index_1 += 1

        for descript in DRUMMING_val:
            self.list_ctrl.SetItem(index_2, 1, descript)
            index_2 += 1


    def on_select(self, event):
        """
        define event
        """
        print("Ooooh! beautifull line, you are happy (scherzo)")
        event.Skip()


if __name__ == "__main__":

    app = wx.App(0)
    frame_1 = MyFrame(None, wx.ID_ANY, "")
    app.SetTopWindow(frame_1)
    frame_1.Show()
    app.MainLoop()
