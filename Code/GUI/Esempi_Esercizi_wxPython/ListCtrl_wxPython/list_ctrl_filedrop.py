#!/usr/bin/python
# -*- coding: UTF-8 -*-

import wx


class MyListCtrl(wx.ListCtrl):
    """
    This is the listControl widget. Note that this wideget has DnDPanel
    parent.
    """
    # ----------------------------------------------------------------------

    def __init__(self, parent):
        """
        Constructor.
        WARNING to avoid segmentation error on removing items by
        listctrl, style must be wx.LC_SINGLE_SEL .
        """
        self.index = None
        self.parent = parent  # parent is DnDPanel class
        #wx.ListCtrl.__init__(self,
                             #parent,
                             #style=wx.LC_REPORT
                             #| wx.LC_SINGLE_SEL,
                             #)
        wx.ListCtrl.__init__(self,
                             parent,
                             style=wx.LC_REPORT,
                             )
        self.populate()
    # ----------------------------------------------------------------------#

    def populate(self):
        """
        populate with default colums
        """
        self.InsertColumn(0, '#', width=30)
        self.InsertColumn(1, ('File Name'), width=200)
        self.InsertColumn(2, ('Duration'), width=200)
        self.InsertColumn(3, ('Media type'), width=200)
        self.InsertColumn(4, ('Size'), width=150)
        self.InsertColumn(5, ('Output file name'), width=200)

    def dropUpdate(self, path, newname=None):
        """
        Update list-control during drag and drop.
        Note that the optional 'newname' argument is given by
        the 'on_col_click' method in the 'FileDnD' class to preserve
        the related renames in column 5 of wx.ListCtrl.

        """
        self.index = self.GetItemCount()

        self.InsertItem(self.index, str(self.index + 1))
        self.SetItem(self.index, 1, path)
        self.SetItem(self.index, 2, 'N/A')
        self.SetItem(self.index, 2, 'N/A')
        self.SetItem(self.index, 3, 'N/A')
        self.SetItem(self.index, 4, 'N/A')
        self.SetItem(self.index, 5, 'N/A')
        self.index += 1


class FileDrop(wx.FileDropTarget):
    """
    This is the file drop target
    """
    # ----------------------------------------------------------------------#

    def __init__(self, window):
        """
        Constructor. File Drop targets are subsets of windows
        """
        wx.FileDropTarget.__init__(self)
        self.window = window  # window is MyListCtr class
    # ----------------------------------------------------------------------#

    def OnDropFiles(self, x, y, filenames):
        """
        When files are dropped, write where they were dropped and then
        the file paths themselves
        """
        for filepath in filenames:
            self.window.dropUpdate(filepath)  # update list control

        return True


class FileDnD(wx.Panel):
    """
    This panel hosts a listctrl for Drag AND Drop actions on which
    you can add new files, select them, remove them, play them,
    sort them or remove them from the list.

    """
    # CONSTANTS:
    YELLOW = '#bd9f00'
    WHITE = '#fbf4f4'  # white for background status bar
    BLACK = '#060505'  # black for background status bar

    def __init__(self, parent):
        """Constructor. This will initiate with an id and a title"""
        self.parent = parent  # parent is the MainFrame
        self.sortingstate = None  # ascending or descending order

        wx.Panel.__init__(self, parent=parent)

        # This builds the list control box:
        self.flCtrl = MyListCtrl(self)  # class MyListCtr
        # Establish the listctrl as a drop target:
        file_drop_target = FileDrop(self.flCtrl)
        self.flCtrl.SetDropTarget(file_drop_target)  # Make drop target.
        # create widgets
        sizer = wx.BoxSizer(wx.VERTICAL)
        sizer.Add((0, 10))
        infomsg = ("Drag one or more files below")
        lbl_info = wx.StaticText(self, wx.ID_ANY, label=infomsg)
        sizer.Add(lbl_info, 0, wx.ALL | wx.EXPAND, 5)
        sizer.Add(self.flCtrl, 1, wx.EXPAND | wx.ALL, 2)
        sizer.Add((0, 10))
        sizer_media = wx.BoxSizer(wx.HORIZONTAL)
        self.btn_play = wx.Button(self, wx.ID_ANY, ("Play"))
        #self.btn_play.SetBitmap(bmpplay, wx.LEFT)
        self.btn_play.Disable()
        sizer_media.Add(self.btn_play, 1, wx.ALL | wx.EXPAND, 2)
        self.btn_remove = wx.Button(self, wx.ID_REMOVE, "")
        self.btn_remove.Disable()
        sizer_media.Add(self.btn_remove, 1, wx.ALL | wx.EXPAND, 2)
        self.btn_clear = wx.Button(self, wx.ID_CLEAR, "")
        self.btn_clear.Disable()
        sizer_media.Add(self.btn_clear, 1, wx.ALL | wx.EXPAND, 2)
        sizer.Add(sizer_media, 0, wx.EXPAND)
        sizer_outdir = wx.BoxSizer(wx.HORIZONTAL)
        self.btn_save = wx.Button(self, wx.ID_OPEN, "...", size=(35, -1))
        self.text_path_save = wx.TextCtrl(self, wx.ID_ANY, "",
                                          style=wx.TE_PROCESS_ENTER |
                                          wx.TE_READONLY
                                          )
        sizer_outdir.Add(self.text_path_save, 1, wx.ALL | wx.EXPAND, 2)
        sizer_outdir.Add(self.btn_save, 0, wx.ALL |
                         wx.ALIGN_CENTER_HORIZONTAL |
                         wx.ALIGN_CENTER_VERTICAL, 2
                         )
        sizer.Add(sizer_outdir, 0, wx.EXPAND)
        self.SetSizer(sizer)


        # Tooltip
        self.btn_remove.SetToolTip(('Remove the selected '
                                     'files from the list'))
        self.btn_clear.SetToolTip(('Delete all files from the list'))
        self.btn_save.SetToolTip(('Set up a temporary folder '
                                   'for conversions'))
        self.btn_play.SetToolTip(('Play the selected file in the list'))
        self.text_path_save.SetToolTip(("Destination folder"))

        # Binding (EVT)
        self.Bind(wx.EVT_BUTTON, self.delete_all, self.btn_clear)
        self.Bind(wx.EVT_BUTTON, self.on_delete_selected, self.btn_remove)
        self.Bind(wx.EVT_LIST_ITEM_SELECTED, self.on_select, self.flCtrl)
        self.Bind(wx.EVT_LIST_ITEM_DESELECTED, self.on_deselect, self.flCtrl)
    # ----------------------------------------------------------------------


    def on_delete_selected(self, event):
        """
        Delete a selected file or a bunch of selected files

        """
        item, indexes = -1, []
        while 1:
            item = self.flCtrl.GetNextItem(item,
                                           wx.LIST_NEXT_ALL,
                                           wx.LIST_STATE_SELECTED)
            indexes.append(item)
            if item == -1:
                indexes.remove(-1)
                break

        if self.flCtrl.GetItemCount() == len(indexes):
            self.delete_all(self)
            return

        #num = 0
        for num in sorted(indexes, reverse=True):
            self.flCtrl.DeleteItem(num)  # remove selected items
            self.flCtrl.Select(num - 1)  # select the previous one
        #self.on_deselect(self)  # deselect removed file

        for x in range(self.flCtrl.GetItemCount()):
            self.flCtrl.SetItem(x, 0, str(x + 1))  # re-load counter
        return
    # ----------------------------------------------------------------------

    def delete_all(self, event, setstate=True):
        """
        Clear all lines on the listCtrl and delete
        self.data list.
        """
        # self.flCtrl.ClearAll()
        self.flCtrl.DeleteAllItems()
        self.btn_play.Disable()
        self.btn_remove.Disable()
        self.btn_clear.Disable()

        if setstate is True:
            self.sortingstate = None
    # ----------------------------------------------------------------------

    def on_select(self, event):
        """
        Selecting line with mouse or up/down keyboard buttons
        """
        index = self.flCtrl.GetFocusedItem()
        item = self.flCtrl.GetItemText(index, 1)
        self.btn_play.Enable()
        self.btn_remove.Enable()
    # ----------------------------------------------------------------------

    def on_deselect(self, event):
        """
        De-selecting a line with mouse by click in empty space of
        the control list
        """
        self.btn_play.Disable()
        self.btn_remove.Disable()
    # ----------------------------------------------------------------------

class WindowFrame(wx.Frame):
    def __init__(self, parent, title):
        wx.Frame.__init__(self, parent, title = title, size=(1024, 540))
        sizerBase = wx.BoxSizer(wx.VERTICAL)
        self.panel=FileDnD(self)
        sizerBase.Add(self.panel, 0, wx.ALL | wx.EXPAND, 10)


if __name__=="__main__":
    app = wx.App(False)
    frame = WindowFrame(None, 'ChatClient')
    frame.Show()
    app.MainLoop()





