#!/usr/bin/env python
# -*- coding: UTF-8 -*-
#
# Thu Aug 21 19:16:20 2014
#

import wx


class MyFrame(wx.Frame):
    def __init__(self, *args, **kwds):
        """
        In questo esempio vengono messi in lista solo i valori del dizionario
        annidato nel dizionario dati, poichè ogni chiave annidata viene
        specificata nel titolo delle colonne della listctrl. l'unica chiave
        usata e messa in lista è quella relativa alla chiave di dati che
        corrisponde al nome personale. Nel mio programma Videomass, ho usato
        questo tecnica per ordinare le liste dal parsing di file xml.
        """

        kwds["style"] = wx.DEFAULT_FRAME_STYLE
        wx.Frame.__init__(self, *args, **kwds)
        self.panel_1 = wx.Panel(self, wx.ID_ANY)
        self.list_ctrl = wx.ListCtrl(self.panel_1, wx.ID_ANY, style=wx.LC_REPORT | wx.SUNKEN_BORDER)

        #set_properties
        self.SetTitle("Demo seleziona catalogo in listctrl")
        self.list_ctrl.SetMinSize((700, 400))

        #__do_layout
        sizer_1 = wx.BoxSizer(wx.VERTICAL)
        grid_sizer_1 = wx.GridSizer(1, 1, 0, 0)
        grid_sizer_1.Add(self.list_ctrl, 1, wx.ALL | wx.EXPAND, 15)
        self.panel_1.SetSizer(grid_sizer_1)
        sizer_1.Add(self.panel_1, 1, wx.EXPAND, 0)
        self.SetSizer(sizer_1)
        sizer_1.Fit(self)
        self.Layout()

        self.Bind(wx.EVT_LIST_ITEM_SELECTED, self.on_select, self.list_ctrl)


        self.set_listctrl()

    def set_listctrl(self):
        """
        setting list_ctrl method
        """
        # dati è un dizionario dalla struttura simile ad un
        # parsing xml
        dati = {'Gino Ballera': {
                        'Corso': 'base', 'Strumento': 'batteria',
                        'Indirizzo': 'via tale n 2 Verona ',
                        'Telefono': '0009990008887'},
                'Massimo Brasier': {
                        'Corso': 'avanzato', 'Strumento': 'batteria',
                        'Indirizzo': 'Via serbia 43 caldiero',
                        'Telefono': '99887766'},
                'Andrea Esse': {
                        'Corso': 'Intermedio', 'Strumento': 'Batteria',
                        'Indirizzo': 'via alfreddo 9 pistoia',
                        'Telefono': '66778899'},
                'Paolo Rossi': {
                        'Corso': 'base', 'Strumento': 'Batteria e piano',
                        'Indirizzo': 'Via Ugo Fosc 45b',
                        'Telefono': '1122334455'},
                'Michele Bianchi': {
                        'Corso': 'Intermedio', 'Strumento': 'Batteria',
                        'Indirizzo': 'Via Monte Bianco 65 verona',
                        'Telefono': '04567890'},
                'Elisa Perfetto': {
                        'Corso': 'Avanzato', 'Strumento': 'batteria e canto',
                        'Indirizzo': 'Via degli Alpini 6 verona',
                        'Telefono': '113322446655'},
                'Margaretha Blovovic': {
                        'Corso': 'Intermedio', 'Strumento': 'Batteria',
                        'Indirizzo': 'Via Altopiano 34 Asiago',
                        'Telefono': '0099008899'},
                'Marta Calma': {
                        'Corso': 'base', 'Strumento': 'Batteria',
                        'Indirizzo': 'Via frat Cervi 112 San Bo',
                        'Telefono': '11332244'}
                }


        self.list_ctrl.InsertColumn(0, 'Nome', width=230)
        self.list_ctrl.InsertColumn(1, 'Corso', width=230)
        self.list_ctrl.InsertColumn(2, 'Strumento', width=230)
        self.list_ctrl.InsertColumn(3, 'Indirizzo', width=350)
        self.list_ctrl.InsertColumn(4, 'Telefono', width=230)


        index = 0
        for name in sorted(dati.keys()):
            # name da tutte le chiavi del dizionario dati
            rows = self.list_ctrl.InsertItem(index, name)
            self.list_ctrl.SetItem(rows, 0, name)

            diz_annidato = dati[name] # da il dizionario annidato nel dizionario dati
            self.list_ctrl.SetItem(rows, 1, diz_annidato["Corso"])
            self.list_ctrl.SetItem(rows, 2, diz_annidato["Strumento"])
            self.list_ctrl.SetItem(rows, 3, diz_annidato["Indirizzo"])
            self.list_ctrl.SetItem(rows, 4, diz_annidato["Telefono"])


    def on_select(self, event):
        """
        define event
        """
        print("Ooooh! beautifull line, you are happy (scherzo)")
        event.Skip()


if __name__ == "__main__":

    app = wx.App(0)
    frame_1 = MyFrame(None, wx.ID_ANY, "")
    app.SetTopWindow(frame_1)
    frame_1.Show()
    app.MainLoop()
