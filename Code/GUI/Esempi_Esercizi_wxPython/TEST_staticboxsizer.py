import wx

class TestPanel(wx.Panel):
    def __init__(self, parent):

        wx.Panel.__init__(self, parent, -1)


        # This gets the recommended amount of border space to use for items
        # within in the static box for the current platform.
        
        bsizer1 = wx.BoxSizer(wx.VERTICAL)
        #self.box_Vcod = wx.StaticBoxSizer(wx.StaticBox(self, wx.ID_ANY,
                                                       #"Video Encoder"),
                                          #wx.VERTICAL)
        #bsizer1.Add(self.box_Vcod, 0, wx.ALL | wx.EXPAND, 5)
        
        sz = wx.StaticBoxSizer(wx.VERTICAL, self, "Box")
        sz.Add(wx.StaticText(sz.GetStaticBox(), wx.ID_ANY,
                            "This window is a child of the staticbox"))
        
        
        
        box = wx.StaticBox(self, wx.ID_ANY, "StaticBox")
        text = wx.StaticText(box, wx.ID_ANY, "This window is a child of the staticbox")
        bsizer1.Add(text, 1, wx.EXPAND|wx.BOTTOM|wx.LEFT|wx.RIGHT|wx.TOP, 20)
        box.SetSizer(bsizer1)
        
        box1 = wx.StaticBox(self, -1, "This is a wx.StaticBox")
        #topBorder, otherBorder = box1.GetBordersForSizer()
        #bsizer1.AddSpacer(topBorder)
        #print(topBorder, otherBorder)

        t1 = wx.StaticText(box1, -1, "As of wxPython 2.9, wx.StaticBox can now be used as a parent like most other wx widgets. This is now the recommended way of using wx.StaticBox.")
        #bsizer1.Add(t1, 1, wx.EXPAND|wx.BOTTOM|wx.LEFT|wx.RIGHT, otherBorder+10)
        bsizer1.Add(t1, 1, wx.EXPAND|wx.BOTTOM|wx.LEFT|wx.RIGHT|wx.TOP, 20)
        box1.SetSizer(bsizer1)

        ## The OLD way.
        box2 = wx.StaticBox(self, -1, "This is a wx.StaticBox using wx.StaticBoxSizer")
        bsizer2 = wx.StaticBoxSizer(box2, wx.VERTICAL)

        t = wx.StaticText(self, -1, "Controls placed \"inside\" the box are really its siblings. This method of using wx.StaticBox is deprecated since wxPython 2.9, and can cause issues on some platforms.")
        bsizer2.Add(t, 1, wx.EXPAND|wx.TOP|wx.LEFT, 10)


        border = wx.BoxSizer(wx.VERTICAL)
        border.Add(box, 1, wx.EXPAND|wx.ALL, 25)
        border.Add(box1, 1, wx.EXPAND|wx.ALL, 25)
        border.Add(bsizer2, 1, wx.EXPAND|wx.ALL, 25)
        self.SetSizer(border)
        
class WindowFrame(wx.Frame):
    def __init__(self, parent, title):
        wx.Frame.__init__(self, parent, title = title, size=(1024, 540))
        sizerBase = wx.BoxSizer(wx.VERTICAL)
        self.panel=TestPanel(self)
        sizerBase.Add(self.panel, 0, wx.ALL | wx.EXPAND, 10)


if __name__=="__main__":
    app = wx.App(False)
    frame = WindowFrame(None, 'ChatClient')
    frame.Show()
    app.MainLoop()

