#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import glob

'''
GLOB and FNMATCH modules

Glob is a general term used to define techniques to match specified patterns according to rules related to Unix shell. Linux and Unix systems and shells also support glob and also provide function glob() in system libraries.

In Python, the glob module is used to retrieve files/pathnames matching a specified pattern. The pattern rules of glob follow standard Unix path expansion rules. It is also predicted that according to benchmarks it is faster than other methods to match pathnames in directories. With glob, we can also use wildcards ("*, ?, [ranges]) apart from exact string search to make path retrieval more simple and convenient.

The glob module finds all the pathnames matching a specified pattern according to the rules used by the Unix shell, although results are returned in arbitrary order. No tilde expansion is done, but *, ?, and character ranges expressed with [] will be correctly matched. This is done by using the os.scandir() and fnmatch.fnmatch() functions in concert, and not by actually invoking a subshell. Note that unlike fnmatch.fnmatch(), glob treats filenames beginning with a dot (.) as special cases. (For tilde and shell variable expansion, use os.path.expanduser() and os.path.expandvars().)

For a literal match, wrap the meta-characters in brackets. For example, '[?]' matches the character '?'.
'''

path = '/home/gianluca/Pubblici/DEVELOPMENTS/master/Videomass/'


def examples(path: str, recursive: bool=False, pattern: str):
    """
    Return a possibly-empty list of path names that match pathname, which must be a string containing a path specification. pathname can be either absolute (like /usr/src/Python-1.5/Makefile) or relative (like ../../Tools/*/*.gif), and can contain shell-style wildcards. Broken symlinks are included in the results (as in the shell). Whether or not the results are sorted depends on the file system. If a file that satisfies conditions is removed or added during the call of this function, whether a path name for that file be included is unspecified.

    If recursive is true, the pattern “**” will match any files and zero or more directories, subdirectories and symbolic links to directories. If the pattern is followed by an os.sep or os.altsep then files will not match.

    Raises an auditing event glob.glob with arguments pathname, recursive.

    Note

    Using the “**” pattern in large directory trees may consume an inordinate amount of time.

    Changed in version 3.5: Support for recursive globs using “**”.


    """

    recurs = '**' if recursive else ''

    if recursive:
        glob.glob(os.path.join(path, '/%s/%s' % (recurs, pattern)),
                  recursive=True)

    else:
        glob.glob(os.path.join(path, pattern), recursive=False)

'''

'''
def globber(paths: list, ext: str, recursive: bool=False):
    """
    https://docs.python.org/3/library/glob.html

    Accept 3 parameters

    Iterates on a generator given from `glob.iglob` method.
    glob treats filenames beginning with a dot (.) as special cases,
    so hidden filenames will not be considered. To convert hidden
    filenames use `fnmatch` module instead of `glob` module.
    """
    recurs = '**' if recursive else ''

    for path in paths:
        for files in glob.iglob(os.path.join(path, recurs, '*.%s' % ext),
                                recursive=recursive
                                ):
            print(files)

ret = globber([path,], 'py')
print(ret)



def fnmatcher():
    """
    https://docs.python.org/3/library/fnmatch.html#fnmatch.fnmatch
    """

    fnmatch.filter(os.listdir(path), '*.svg')  # necessita di una lista di files

    for files in os.listdir(path):
        if fnmatch.fnmatch(files, '*.svg'):
            print(files)
