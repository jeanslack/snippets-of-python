#!/usr/bin/env python3
# -*- coding: UTF-8 -*-



######################## Parametri di tipo string male formattati
"""
Questa tecnica diventa utile nel caso in cui una lunga stringa sia mal 
formattata con più spazi o tablature e si volesse avere una formattazione
corretta con i soli spazi.
"""

import string

bad_string = "ffmpeg -i 	'/home/gianluca/Video/SkidRow/*.mp4' 		-loglevel error  -pass 1 -an   -vcodec mpeg4 -vb 1500k      -threads 4 -t 20 -f rawvideo -y /dev/null    && /home/gianluca/Video/SkidRow -i 'ffmpeg/*.mp4' -loglevel   error -pass 2 -vcodec mpeg4 -vb 1500k      -acodec ac3 -ab 192k -ar 44100 -ac 2 -threads 4 -t 20 -y '/home/gianluca/Modelli/*.avi'  "

# diventa una lista, ogni singola parola diventa un elemento della lista:
is_list = string.split(bad_string) 

print is_list

# ogni elemento della lista separato da uno spazio e unito in una 
# singola stringa:
good_string = string.join(is_list) 

print good_string

############  Tecnica implementata su videomass per la valutazione di wildcard

write = input("scrivi una lista di parole semplici "
                  "separate da spazi bianchi  "
                  )
stringa = write

if stringa in string.whitespace: # se ha degli spazi bianchi o tabulazioni vuote
    file_support = "*.*"
else:
    li = stringa.split() # diventa una lista con parole separate

    new = [] # nuova lista vuota

    for a in li:
        new.append("*.%s;" % (a))

#stringher = string.join(new) # la lista diventa una stringa
#f = len(stringher) # mi da la lunghezza totale della stringa

part1 = ''.join(new) # la lista diventa una stringa
#part1 = "%s" % stringher[:f - 1] # rimuovo l'ultimo carattera della stringa

# rimuove tutti gli spazi bianchi anche multipli in tutta la 
# lunghezza della stringa:
part2 = "".join(part1.split()) 


wildcard = "Audio source (%s)|%s|" % (part1, part2), "All files (*.*)|*.*"

print(wildcard)
