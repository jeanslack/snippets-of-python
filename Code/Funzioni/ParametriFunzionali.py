#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#### PARAMETRI POSIZIONALI DI FUNZIONE

#-------------- argomenti con nome
'''
Gli argomenti con nome sono obbligatori nella chiamata a funzione
e dovranno essere tanti quanti vengono stabiliti nella sucessione 
dei parametri (signature).
'''

def StampaParita(x):
    '''
    verifica numeri pari e dispari con argomento unico mandatorio
    '''
  if x%2 == 0:
    print x, "e' pari"
  else:
    print x, "e' dispari"
    
StampaParita(3)
# ---------------------------------#


def crivello(n):
    """
    Di seguito l'implementazione del Crivello di Eratostene Adattato alla 
    versione 2.6.4 di Python ma compatibile con Python3.

    Funzionamento:

    -1 Viene creata una lista num di interi, da 0 fino al numero n;
    -2 Si sostituisce l'elemento in posizione uno con uno zero;
    -3 Per ogni numero x da 0 alla radice quadrata di n si controlla che 
       l'elemento in posizione num[x] sia diverso da 0;
    -4 Se lo è si cambiano tutti i suoi multipli con degli 0;
    -5 Si eliminano tutti gli 0 dalla lista;
    -6 Si ritorna la lista.
    """
    num = list(range(n+1))
    num[1] = 0
    for x in range(int(n**0.5)+1):
        if num[x] != 0:
            num[2*x::x] = [0]*(int(n/x)-1)

    return list(filter(None, num))

g = crivello(20)

print(g)

# ---------------------------------#

def sottrazione(minuendo, sottraendo):
    '''
    Gli argomenti che verranno passati alla funzione seguiranno
    l'ordine dei parametri con i quali è stata definita la signature
    della funzione. Se non si vuole seguire l'ordine della signature
    gli argomenti dovranno essere specificati esplicitamente con gli 
    stessi nomi dei parametri seguiti dal segno di uguale e da una
    assegnazione.
    '''
    differenza = minuendo - sottraendo
    print(differenza)
    
sottrazione(12,2)# segue ordinazione signature

sottrazione(sottraendo=2, minuendo=12)# non segue l'ordine signature


# ------------- Argomenti opzionali ed argomenti con nome
'''
Python permette agli argomenti delle funzioni di avere un valore predefinito; 
se la funzione è chiamata senza l'argomento, l'argomento prende il suo valore 
predefinito. Inoltre gli argomenti possono essere specificati in qualunque 
ordine usando gli argomenti con nome.
'''
def funz_1(param0,param1=23,param2=27):
    '''
    param0 è l'unico argomento mandatorio che dovrà essere specificato
    nella chiamata a funzione. i rimanenti sono opzionali.
    '''
    print('\nparola chiave: %s\n' % param0)
    print(param1, param2)
    print(str(param1+param2))


#funz_1('bella oi como va',param2='stringa')

#{param0:param0, param1:23, param2:27}


# ------------- argomenti variadic (*varags) e keywards (**kwargs)
'''
Per approfondimenti vedere pag 237 Guida Python (Marco Buttu)
'''

def varARGS(*varargs):
    '''
    '''
    print(varargs)
    for count, args in enumerate(varargs):
        print("%s -- %s" %(count,args))

#varARGS('popolo','sentore',12,['a','b','c'])

def kwARGS(**kwargs):
    '''
    '''
    print(kwargs)
    for k,v in kwargs.items():
        print('%s -- %s' %(k,v))

#kwARGS(cielo='stelle',mare='profondo',n=24, lista=[1,2,3,4,5])

def all_args(var1,var2,*varargs,**kwargs):
    '''
    questo dovrebbe essere l'ordine di precedenza della signature nel caso 
    si volesse usare un mixing di args, varargs e keywords args.
    '''
    print(var1,var2,varargs,kwargs)

all_args('ciao','mondo',1,2,3,4,5,uno=1,due=2,tre=3)



