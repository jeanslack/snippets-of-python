#!/usr/bin/env python3
# -*- coding: utf-8 -*-

############################## FUNZIONI GENERATORE (generator)
# pag 262 programmare con python (marco buttu)


'''
Il protocollo di iterazione consente di recuperare, uno alla volta, gli
elementi da un oggetto iterabile senza che questo venga caricato interamente
in memeoria RAM. Possiamo ottenere lo stesso comportamento definendo un 
particolare tipo di funzione, detta funzione generatore (generator).
'''

def HOTPO_func(n):#Funzione normale
    '''
    considerando questa normale funzione dove il controllo al chiamante 
    viene restituito con return, gli elementi dell'iterazione rimangono
    in memoria.
    '''
    mylist = []
    while n != 1:
        mylist.append(n)
        n = n // 2 if n % 2 == 0 else 3*n +1
    else:
        mylist.append(1)
        return mylist

    
for i in HOTPO_func(17):
    print(i)
    

def HOTPO_Generator(n):# Generator
    '''
    Questa tipologia di funzione restituisce un generatore e viene chiamata 
    con il metodo __next__(). Dal momento che un generatore è un itaratore 
    l'istruzione for consente di iterare su di esso. Questa volta il
    controllo al chiamante viene restituito con l'istruzione yield.
    '''
    while n != 1:
        yield n
        # n diviso due (non float) ma solo se n è pari (cioè 0) altrimenti
        # 3 per n più 1:
        n = n // 2 if n % 2 == 0 else 3*n +1
    else:
        yield 1
#----------        
list(HOTPO_Generator(10)) # iterable items in list
#----------
# Con il metodo __next__() possiamo produrre gli elementi della sequenza 
# uno alla volta su richiesta finchè solleverà StopIteration al termine.
f = HOTPO_Generator(2)
f.__next__()

# oppure iterando sull'iteratore
for i in HOTPO_Generator(17):
    print(i)
    
# il modulo itertools contiene funzioni speciali per manipolare gli iterabili
# < https://stackoverflow.com/questions/231767/what-does-the-yield-keyword-do >
import itertools

# saltare una parte iniziale degli elementi del generator:
for i in itertools.dropwhile(lambda num: num > 4, HOTPO_Generator(17)):
    print(i)
    
# saltare un dato intervallo di elementi
for i in itertools.islice(HOTPO_Generator(17), 10, None):
    # primo elemento è l'iteratore (generator), il secondo l'indice
    # dell'elemento iniziale e il terzo di quello finale (che in questo
    # caso è None)
    print(i)

# vedere l'ordine di permutazione degli elementi
for i in itertools.permutations([1,2,3,4]):
    print(i)
    
############################## COROUTINE

############################## GENERATOR EXPRESSION
