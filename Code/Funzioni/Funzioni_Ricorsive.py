#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
La ricorsione è una pratica comune a ogni linguaggio di programmazione, 
non solo nella sostanza ma anche nella forma cioè con la medesima struttura 
sintatica. Così, come una funzione può essere chiamata da un'altra funzione, 
una medesima funzione può richiamare sé stessa più volte. La ricorsione è 
lecita quando non ricorre in modo infinito:
    
    def funz_senzafine():
        print('senza limite')
        funz_senzafine()
    
tuttavia risulta più lenta ed utilizza più memoria se paragonata ad un 
normale ciclo di iterazione. In sostanza se non ci sono validi motivi per 
utilizzarla se ne sconsiglia l'uso in favore dei cicli. 

Le seguenti funzioni riportano piccoli esempi di ricorsione non infinita
e le corrispettive funzioni con cicli di iterazione.
"""


def NRigheVuoteRicorsiva(n):
    '''una funzione ricorsiva'''
    if n > 0:
        print()
        NRigheVuote(n-1)

NRigheVuoteRicorsiva(4)

def NRigheVuote(n):
    '''non ricorsiva'''
    while n > 0:
        n-=1
        print()

NRigheVuote(4)

#-----------------------------------------------#

def ContoAllaRovesciaRicorsivo(n):
    '''
    Accetta solo interi e valuta la lunghezza della lista.
    Se sono presenti elementi in lista scala ogni intero di uno
    fino al valore 0 in modo ricorsivo.
    '''
    if n == 0:
        print("Partenza!")
    else:
        print(n)
        ContoAllaRovescia(n-1)

ContoAllaRovesciaRicorsivo(3)


def ContoAllaRovesciaRicorsivo(n):
    '''
    equivalente di ContoAllaRovescia senza ricorsione.
    '''
    print(n)
    while n > 0:
        n-=1
        print(n)
    if n == 0:
        print('partenza!')
        
ContoAllaRovescia(3)

#-----------------------------------------------#

def sumseq_recursive(seq):
    '''
    Accetta solo interi e valuta se la lista non ha elementi che nel caso
    ritornerà falso 0. Altrimenti esegue la somma fino ad esaurimento
    elementi, avanzando di un elemento alla volta che passerà a se stessa.
    '''
    if not seq:
        return 0
    return seq[0] + sumseq_recursive(seq[1:])

sumseq_recursive([12,13,14,15])

def sumseq(seq):
    '''
    Questa funzione è l'equivalente non ricursiva.
    '''
    total = 0
    for i in seq:
        total +=i
    return total

sumseq([12,13,14,15])

sum([12,13,14,15]) # ...o usando la classe sum di python

#-----------------------------------------------#

# Fibonacci's series:
# The first number of the sequence is 0, the second number is 1, 
# and each subsequent number is equal to the sum of the previous 
# two numbers of the sequence itself, yielding the sequence 
# 0, 1, 1, 2, 3, 5, 8, etc.

result = []
def recursive_fib(n,a=0,b=1):
    '''
    calcola la serie di Fibonacci avente int(n) positivo in modo ricorsivo
    '''
    if not isinstance(n,int):#valuto se intero o meno
        print('Must be a positive number from int(0) to infinity')
        return
    
    if a < n:
        result.append(a)
        a,b = b,a+b
        return recursive_fib(n,a,b) 

    #return result

recursive_fib(100)
print(result)

def fib(n):
    '''
    calcola la serie di Fibonacci avente int(n) positivo
    '''
    if not isinstance(n,int):#valuto se intero o meno
        return 'Must be a positive number from int(0) to infinity'
    
    a,b = 0,1
    result = []
    while a < n:
        result.append(a)
        a,b = b,a+b
    return result

print(fib(45))
#-----------------------------------------------#
