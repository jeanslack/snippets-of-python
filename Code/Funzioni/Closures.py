#!/usr/bin/env python3

"""
LE CLOSURES (https://www.programiz.com/python-programming/closure)

Prima di addentrarci su cosa siano le closures dobbiamo capire cos'è
una nested function (funzione annidata), cioè una funzione dentro a una
funzione, e cos'è una nonlocal variable.

Come sappiamo una variabile locale appartiene al local scope di una funzione 
nella quale è stata inizializzata ed è "vista" solo nello scope di quella 
funzione.

L'uso della keyword nonlocal è molto simile alla keyword global ed è usata
per dichiarare che una variabile dentro a una funzione annidata non è locale
a essa, il che significa che si trova anche nella funzione enclosing.

"""

def outer_function():
    a = 5 
    def inner_function():
        nonlocal a
        a = 10
        print("Inner function: ",a)
    inner_function()
    print("Outer function: ",a)

outer_function()# Output: Inner function:  10 Outer function:  10

"""
------------------------------
    DEFINIZIONE DI UNA CLOSURE:
    
Cosa succede se in outer_function facciamo ritornare la funzione inner_function
anzichè chiamarla esplicitamente?
"""

def outer_function(a):
    def inner_function():
        print("Inner function: ",a)
    return inner_function

another = outer_function(5)
another()

"""
La funzione outer_function() è stata chiamata con l'argomento 5 e la funzione 
restituita era associata al nome another. Chiamando another(), fuori dalla 
funzione, il messaggio veniva ancora ricordato anche se avevamo già finito 
di eseguire la funzione outer_function().

Questa tecnica con cui alcuni dati (5) vengono allegati al codice si chiama 
closure in Python.

-------------------------------
    QUANDO ABBIAMO UNA CLOSURE?

Come visto dall'esempio precedente, abbiamo una closure in Python quando 
una funzione annidata fa riferimento a un valore nel suo enclosing scope
(ambito di chiusura).

I criteri per creare una closure sono sommarizzati nei punti seguenti:

    - Dobbiamo avere una funzione annidata.
    - La funzione annidata deve fare riferimento a un valore definito nella 
      funzione enclosing.
    - La funzione enclosing deve ritornare la funzione annidata

-----------------------------    
    QUANDO USARE LE CLOSURES?
    
Per cosa sono indispensabili? Le closures possono far evitare di usare valori
globali nel corpo del codice e provvedono qualche forma di occultamento dati.
Inoltre, possono anche essere usate per soluzioni a problemi di tipo object
oriented.

Quando ci sono pochi metodi implementati in una classe (uno solitamente),
le closures possono fornire una soluzione alternativa più elegante.
Ma quando il numero dei metodi e attributi diventa grande, allora è meglio
implementare una classe. 

Di seguito un semplice esempio dove è preferibile usare una closure in 
luogo di una classe e alla creazione di oggetti.

"""

def make_multiplier_of(n):
    def multiplier(x):
        return x * n
    return multiplier

# Multiplier of 3
times3 = make_multiplier_of(3)

# Multiplier of 5
times5 = make_multiplier_of(5)

# Output: 27
print(times3(9))

# Output: 15
print(times5(3))

# Output: 30
print(times5(times3(2)))

#NOTE Anche i decoratori fanno ampio uso delle closures.




