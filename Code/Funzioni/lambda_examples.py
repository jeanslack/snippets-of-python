#!/usr/bin/env python3


#------------------------------------------------------------------ 1)
# la variabile check contiene una funzione lambda, la quale accetta
# un argomento di tipo string o di tipo int (a seconda se vengono immessi
# caratteri digit o meno) e formata da un'espressione con la
# quale valuta con l'argomento passato l'appartenenza a una data lista.
# Se l'argomento esiste, viene stampato l'indice di appartenenza alla
# lista; se non esiste l'output dato sarà `None` e l'argomento verrà
# inserito nella lista come nuovo elemento.

lista = [10,12,13,'dieci','dodici','tredici'] # lista ipotetica

check = lambda x: lista.index(x) if x in lista else lista.append(x)

i = input('cerca nella lista il valore:  >>  ')
if i.isdigit():# se solo stringhe di numeri
    i = int(i)#cambio da stringa a intero

print(check(i)) # chiama la funzione e stampa

quest = input('mostrare il contenuto della lista aggiornato? [y/n]:  ')
if quest in ['y','Y','s','S']:
    print(lista)


#------------------------------------------------------------------ 2)
# Trova le mancanze in una lista di interi fino all'intero
# più grande della sequenza.

key = lambda seq: [x for x in range(max(seq)) if x not in seq]

key([8,4,6,1,3,0,13]) # che darà [2, 5, 7, 9, 10, 11, 12]


#------------------------------------------------------------------ 3)

# Ordina la lista dal primo elemento di ogni lista annidata (o dal secondo
# o dal terzo, etc. basta cambiare l'indice s[index])

li = [(23, 'xm', 'am'), (20, 'ca', 'ba'), (15, 'an', 'cum', 'po')]

# To leaving untouched original list you have to use sorted() function:
new = sorted(li, key=lambda s: s[0])  # this create a new list

# change original list in-place using sort method. Sort is a specific method
# for list only (dir(list)). For other data structures as frozen you have to
# use sorted function.
li.sort(key=lambda s: s[0])


