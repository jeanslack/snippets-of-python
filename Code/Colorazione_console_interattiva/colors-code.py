#!/usr/bin/env python
# -*- coding: utf-8 -*-

#################################################################
#  MODULO CODICI DEI COLORI PER COLORARE IL PROMPT di PYTHON    
#
#  ISRUZIONI: importare il modulo con 'import colors'
#             Assegnare il modulo alle stringhe interessate con:
#	      print colors.COLORE_SCELTO + "stringhe di testo"
#             Tornare ai valori predefiniti con:
#	      "stinghe di testo" + colors.DEFAULT
#################################################################


# Colora i caratteri (foreground):

black = "\033[30m"
red = "\033[31m"
green = "\033[32m"
yellow = "\033[33m"
blue = "\033[34m"
magenta = "\033[35m"
cyan = "\033[36m"
white = "\033[37m" 


# colora caratteri Bold: 

bold = "\033[1m" # colore predefinito bold
black_b = "\033[30;1m"
red_b = "\033[31;1m"
green_b = "\033[32;1m"
yellow_b = "\033[33;1m"
blue_b = "\033[34;1m"
magenta_b = "\033[35;1m"
cyan_b = "\033[36;1m"
grey_b = "\033[30;1m" # grigio scuro bold
white_b = "\033[37;1m" # Bianco bold


# Colorare lo sfondo dei caratteri (Background)

BLACK="\033[40m"
RED="\033[41m"
GREEN="\033[42m"
YELLOW="\033[43m"
BLUE="\033[44m"
MAGENTA="\033[45m"
CYAN="\033[46m"
WHITE="\033[47m"


# Speciali

DIM="\033[2m" # Sbiadisce, opacizza
UNDERLINE="\033[4m" # sottolinea
REVERSE="\033[7m" # evidenzia: gira i colori

# Torna allo stato iniziale:

DEFAULT="\033[0m"

