# -*- coding: utf-8 -*-


#################################################################
#  MODULO CODICI COLORI PER COLORARE IL PROMPT di PYTHON 
#
#  DATE:      05/10/2010   
#
#  ISRUZIONI: importare il modulo con 'import color_markup_string as cms'
#             Assegnare il modulo alle stringhe interessate con:
#	      print cms.nomefunzione("<colore>stringhe di testo</colore>")
#             
#  NOTE:      Autore: Gianluca Pernigotto <jeanlucperni@gmail.com>
#             Questa è una release migliorata, più completa dell'originale
#             rilasciata inizialmente da Simone Cansella <matto.scacco@gmail.com>. 
#             L'ho personalmente testata su Debian Squeeze con python 2.6.6
#             
#    
# Copyright (C) 2008  Gianluca Pernigotto
#                       <jeanlucperni@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#######################################################################


##################### colors    (ex: <color> = normal , <color_b> = bold , <color_d> = dim)
def color (string):
	colors = {"default":0, "black":30, "red":31, "green":32, "yellow":33,
          "blue":34,"magenta":35, "cyan":36, "white":37, "black":38,
          "black":39, "black_b":"30;1", "red_b":"31;1", "green_b":"32;1", "yellow_b":"33;1",  
          "blue_b":"34;1","magenta_b":"35;1", "cyan_b":"36;1", "white_b":"37;1", "black_b":"38;1",
          "black_b":"39;1","black_d":"30;2", "red_d":"31;2", "green_d":"32;2", "yellow_d":"33;2",  
          "blue_d":"34;2","magenta_d":"35;2", "cyan_d":"36;2", "white_d":"37;2", "black_d":"38;2",
          "black_d":"39;2"} #33[%colors%m
    
	for color in colors:
        	color_string = "\033[%sm" % colors[color]
        	string = string.replace("<%s>" % color, color_string).replace("</%s>" % color, "\033[0m")
    
	return string



#################### underline  (ex: <color> = normal , <color_b> = bold , <color_d> = dim)
def underline (string):
	colors = {"default":0, "black":30, "red":31, "green":32, "yellow":33,
          "blue":34,"magenta":35, "cyan":36, "white":37, "black":38,
          "black":39, "black_b":"30;1", "red_b":"31;1", "green_b":"32;1", "yellow_b":"33;1",  
          "blue_b":"34;1","magenta_b":"35;1", "cyan_b":"36;1", "white_b":"37;1", "black_b":"38;1",
          "black_b":"39;1","black_d":"30;2", "red_d":"31;2", "green_d":"32;2", "yellow_d":"33;2",  
          "blue_d":"34;2","magenta_d":"35;2", "cyan_d":"36;2", "white_d":"37;2", "black_d":"38;2",
          "black_d":"39;2"} #33[%colors%m
    
	for underline in colors:
        	color_string = "\033[%s;4m" % colors[underline]
        	string = string.replace("<%s>" % underline, color_string).replace("</%s>" % underline, "\033[0m")
    
	return string


#################### background colors (ex: <color> = normal , <color_b> = bold , <color_d> = dim)



                                       # black
def blackbgr (string):
	colors = {"default":0, "black":30, "red":31, "green":32, "yellow":33,
          "blue":34,"magenta":35, "cyan":36, "white":37, "black":38,
          "black":39, "black_b":"30;1", "red_b":"31;1", "green_b":"32;1", "yellow_b":"33;1",  
          "blue_b":"34;1","magenta_b":"35;1", "cyan_b":"36;1", "white_b":"37;1", "black_b":"38;1",
          "black_b":"39;1","black_d":"30;2", "red_d":"31;2", "green_d":"32;2", "yellow_d":"33;2",  
          "blue_d":"34;2","magenta_d":"35;2", "cyan_d":"36;2", "white_d":"37;2", "black_d":"38;2",
          "black_d":"39;2"}

	for blackbgr in colors:
        	color_string = "\033[40;%sm" % colors[blackbgr]
        	string = string.replace("<%s>" % blackbgr, color_string).replace("</%s>" % blackbgr, "\033[0m")
    
	return string

                                       # red
def redbgr (string):
	colors = {"default":0, "black":30, "red":31, "green":32, "yellow":33,
          "blue":34,"magenta":35, "cyan":36, "white":37, "black":38,
          "black":39, "black_b":"30;1", "red_b":"31;1", "green_b":"32;1", "yellow_b":"33;1",  
          "blue_b":"34;1","magenta_b":"35;1", "cyan_b":"36;1", "white_b":"37;1", "black_b":"38;1",
          "black_b":"39;1","black_d":"30;2", "red_d":"31;2", "green_d":"32;2", "yellow_d":"33;2",  
          "blue_d":"34;2","magenta_d":"35;2", "cyan_d":"36;2", "white_d":"37;2", "black_d":"38;2",
          "black_d":"39;2"}

	for redbgr in colors:
        	color_string = "\033[41;%sm" % colors[redbgr]
        	string = string.replace("<%s>" % redbgr, color_string).replace("</%s>" % redbgr, "\033[0m")
    
	return string

                                      # green
def greenbgr (string):            
	colors = {"default":0, "black":30, "red":31, "green":32, "yellow":33,
          "blue":34,"magenta":35, "cyan":36, "white":37, "black":38,
          "black":39, "black_b":"30;1", "red_b":"31;1", "green_b":"32;1", "yellow_b":"33;1",  
          "blue_b":"34;1","magenta_b":"35;1", "cyan_b":"36;1", "white_b":"37;1", "black_b":"38;1",
          "black_b":"39;1","black_d":"30;2", "red_d":"31;2", "green_d":"32;2", "yellow_d":"33;2",  
          "blue_d":"34;2","magenta_d":"35;2", "cyan_d":"36;2", "white_d":"37;2", "black_d":"38;2",
          "black_d":"39;2"}

	for greenbgr in colors:
        	color_string = "\033[42;%sm" % colors[greenbgr]
        	string = string.replace("<%s>" % greenbgr, color_string).replace("</%s>" % greenbgr, "\033[0m")
    
	return string


                                     # yellow
def yellowbgr (string):            
	colors = {"default":0, "black":30, "red":31, "green":32, "yellow":33,
          "blue":34,"magenta":35, "cyan":36, "white":37, "black":38,
          "black":39, "black_b":"30;1", "red_b":"31;1", "green_b":"32;1", "yellow_b":"33;1",  
          "blue_b":"34;1","magenta_b":"35;1", "cyan_b":"36;1", "white_b":"37;1", "black_b":"38;1",
          "black_b":"39;1","black_d":"30;2", "red_d":"31;2", "green_d":"32;2", "yellow_d":"33;2",  
          "blue_d":"34;2","magenta_d":"35;2", "cyan_d":"36;2", "white_d":"37;2", "black_d":"38;2",
          "black_d":"39;2"}

	for yellowbgr in colors:
        	color_string = "\033[43;%sm" % colors[yellowbgr]
        	string = string.replace("<%s>" % yellowbgr, color_string).replace("</%s>" % yellowbgr, "\033[0m")
    
	return string


                                    # blue
def bluebgr (string):            
	colors = {"default":0, "black":30, "red":31, "green":32, "yellow":33,
          "blue":34,"magenta":35, "cyan":36, "white":37, "black":38,
          "black":39, "black_b":"30;1", "red_b":"31;1", "green_b":"32;1", "yellow_b":"33;1",  
          "blue_b":"34;1","magenta_b":"35;1", "cyan_b":"36;1", "white_b":"37;1", "black_b":"38;1",
          "black_b":"39;1","black_d":"30;2", "red_d":"31;2", "green_d":"32;2", "yellow_d":"33;2",  
          "blue_d":"34;2","magenta_d":"35;2", "cyan_d":"36;2", "white_d":"37;2", "black_d":"38;2",
          "black_d":"39;2"}

	for bluebgr in colors:
        	color_string = "\033[44;%sm" % colors[bluebgr]
        	string = string.replace("<%s>" % bluebgr, color_string).replace("</%s>" % bluebgr, "\033[0m")
    
	return string

                                   # magenta
def magentabgr (string):            
	colors = {"default":0, "black":30, "red":31, "green":32, "yellow":33,
          "blue":34,"magenta":35, "cyan":36, "white":37, "black":38,
          "black":39, "black_b":"30;1", "red_b":"31;1", "green_b":"32;1", "yellow_b":"33;1",  
          "blue_b":"34;1","magenta_b":"35;1", "cyan_b":"36;1", "white_b":"37;1", "black_b":"38;1",
          "black_b":"39;1","black_d":"30;2", "red_d":"31;2", "green_d":"32;2", "yellow_d":"33;2",  
          "blue_d":"34;2","magenta_d":"35;2", "cyan_d":"36;2", "white_d":"37;2", "black_d":"38;2",
          "black_d":"39;2"}

	for magentabgr in colors:
        	color_string = "\033[45;%sm" % colors[magentabgr]
        	string = string.replace("<%s>" % magentabgr, color_string).replace("</%s>" % magentabgr, "\033[0m")
    
	return string


                                  # cyan
def cyanbgr (string):            
	colors = {"default":0, "black":30, "red":31, "green":32, "yellow":33,
          "blue":34,"magenta":35, "cyan":36, "white":37, "black":38,
          "black":39, "black_b":"30;1", "red_b":"31;1", "green_b":"32;1", "yellow_b":"33;1",  
          "blue_b":"34;1","magenta_b":"35;1", "cyan_b":"36;1", "white_b":"37;1", "black_b":"38;1",
          "black_b":"39;1","black_d":"30;2", "red_d":"31;2", "green_d":"32;2", "yellow_d":"33;2",  
          "blue_d":"34;2","magenta_d":"35;2", "cyan_d":"36;2", "white_d":"37;2", "black_d":"38;2",
          "black_d":"39;2"}

	for cyanbgr in colors:
        	color_string = "\033[46;%sm" % colors[cyanbgr]
        	string = string.replace("<%s>" % cyanbgr, color_string).replace("</%s>" % cyanbgr, "\033[0m")
    
	return string


                                 # white
def whitebgr (string):            
	colors = {"default":0, "black":30, "red":31, "green":32, "yellow":33,
          "blue":34,"magenta":35, "cyan":36, "white":37, "black":38,
          "black":39, "black_b":"30;1", "red_b":"31;1", "green_b":"32;1", "yellow_b":"33;1",  
          "blue_b":"34;1","magenta_b":"35;1", "cyan_b":"36;1", "white_b":"37;1", "black_b":"38;1",
          "black_b":"39;1","black_d":"30;2", "red_d":"31;2", "green_d":"32;2", "yellow_d":"33;2",  
          "blue_d":"34;2","magenta_d":"35;2", "cyan_d":"36;2", "white_d":"37;2", "black_d":"38;2",
          "black_d":"39;2"}

	for whitebgr in colors:
        	color_string = "\033[47;%sm" % colors[whitebgr]
        	string = string.replace("<%s>" % whitebgr, color_string).replace("</%s>" % whitebgr, "\033[0m")
    
	return string


























