#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

import re

# cerca e consenti tutti i caratteri alfanumerici, gli spazi e i tab; non
# consentire tutti i metacaratteri tranne questi ° - , ! [ ] ; ( ) @ # .
stringa = 'stringa da valutare'
check = bool(re.search(r"^(?:[\w\°\-\,\!\[\]\;\(\)\@\#\.\ ]{1,255}$)*$",
                       stringa))
if check is not True:
    print('invalid')

# cerca e consenti tutti i caratteri alfanumerici, gli spazi/tab, i
# metacaratteri tranne questi ^` ~ " # ' % & * : < > ? / \ { | }
stringa = 'stringa da valutare ^'
check = bool(re.search(r"^(?:[^\'\^\`\~\"\#\'\%\&\*\:\<\>\?\/\\\{\|\}]"
                       r"{1,255}$)*$", stringa))

if check is not True:
    print('invalid')

# trova le ripetizioni sparse su una frase e le mette in lista con un numero
# di tuple quante sono poste dall'ordine sparso.
string = 'My # Name is JoHn ##'
occur = re.findall(r'((\#)\#*)', string)

if len(occur) > 1:  # if more '#' occurrences between sentence.
    print('invalid')

# trova tutte le ripetizioni e le mette in lista
string = 'My # Name is JoHn ##'
occur = re.findall(r"(\#)", string)

if len(occur) > 1:  # if more '#' occurrences between sentence.
    print('invalid')

# Cerca e Sostituisci con uno zero tutti i caratteri cancelletto.
string 'frase # fatta # a mano #####'
sub = re.sub('#','0', string)
print(sub)
