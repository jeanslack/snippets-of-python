#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""             LE LISTE:

Le liste sono delle sequenze mutabili ordinate (cioè conservano l'ordine
dato) che possono contenere qualsiasi tipo di oggetto.
E' possibile creare liste vuote, cioè senza alcun contenuto, vedi esempi sotto.
Oltre al metodo di assegnazione list=[value] tipico delle liste, è possibile
fare assegnazioni multiple automatizzate, vedi list comprehension più in basso.

NOTA: Le spiegazioni in basso non pretendono di essere esaurienti ma vengono
estese le più comuni negli usi da me riportati.
Per maggiori info, dare dir(list) sulla console e help(metodo) per ogni più
esauriente spiegazione.

>>> dir(array)
['__add__', '__class__', '__contains__', '__delattr__', '__delitem__', '__dir__',
 '__doc__', '__eq__', '__format__', '__ge__', '__getattribute__', '__getitem__',
 '__gt__', '__hash__', '__iadd__', '__imul__', '__init__', '__init_subclass__',
 '__iter__', '__le__', '__len__', '__lt__', '__mul__', '__ne__', '__new__',
 '__reduce__', '__reduce_ex__', '__repr__', '__reversed__', '__rmul__',
 '__setattr__', '__setitem__', '__sizeof__', '__str__', '__subclasshook__',
 'append', 'clear', 'copy', 'count', 'extend', 'index', 'insert', 'pop', 'remove',
 'reverse', 'sort']
"""
#----------------------------------------------------------------------------#

############# ESEMPI DI CREAZIONE LISTE:

lista = [] # creo lista vuota
# oppure:
lista = list() # alternativamente, creo una lista vuota
# fonti : http://docs.python.it/html/tut/node7.html

# creo una lista contenente valori misti (stringhe, interi e variabili):
variabile = 'valore variabile'# var di passaggio per contenuto lista1
lista1 = ['stringa1', 'stringa2', 22, variabile, 'etc']

# List comprehension che crea tutte le combinazioni tra ABC e 123
# come la tavola delle battaglie navali o le mappe delle carte topografiche:
table = [c+n for c in 'ABC' for n in '123']
#----------------------------------------------------------------------------#

############# METODI DI ACCESSO ALLE LISTE:

# stampa tutti gli elementi della lista:
print lista1

# stampa la lunghezza della lista (parte da 1 e non da 0):
print len(lista1)

# stampa in ordine numerico e alfabetico la lista:
print sorted(lista1)
#----------------------------------------------------------------------------#

############## ACCESSO ALLE LISTE ATTRAVERSO GLI INDICI E GLI SLICING

# Stampa l'indice dell'elemento 'etc' della lista. Nota che se un elemento
# non è presente in lista il metodo index() solleverà un errore ValueError:
lista1.index('etc')

# INDICIZZAZIONI:
# Stampa tutti gli elementi della lista con l'uso dello slicing.
# Se chiedete un sezionamento senza specificare alcun indice, tutti gli
# elementi della lista saranno inclusi. L'oggetto restituito però non è la
# lista li originale ma è una nuova lista che contiene gli stessi elementi.
# li[:] è un'abbreviazione per creare una copia completa di una lista:
lista1[:] # ['stringa1','stringa2', 22, variabile,'etc']

# Stampa il primo elemento. Lo zero è sempre il primo elemento in PYTHON:
lista1[0]

# Stampa il quarto elemento della lista1:
lista1[3]

# Gli indici negativi contano all'indietro a cominciare dal primo elemento
# della lista cioè [0] :
lista1[-1] # risultato: 'etc'
lista1[-3] # risulta: 22

# SLICING:
lista1[1:3] #  ['stringa2', 22]
lista1[1:-1] # ['stringa2', 22, 'valore variabile']
lista1[0:3] # ['stringa1', 'stringa2', 22]

lista1[:3] # dal primo(0°) al 2° elemento
lista[3:] # dal 3° all'ultimo elemento
#----------------------------------------------------------------------------#

############## TESTARE SE GLI ELEMENTI SONO IN LISTA

# I test consistono nel verificare se un oggetto è in lista. Questo può essere
# fatto esplicitamente con il nome dell'oggetto. Il risultato è sempre un
# valore booleano True o False:
22 in lista1
print ('mela' in lista1)
print ('mela' not in lista1)
#----------------------------------------------------------------------------#

############## AGGIUNGERE O MODIFICARE ELEMENTI IN LISTA

# Aggiunge un nuovo record e lo posiziona sempre alla fine degli elementi:
lista1.append('mela')

# Modifica di un elemento in lista. Nota che se non conosci l'indice, che è
# sempre un intero, puoi cercarlo con il metodo index(), vedi più sopra:
lista1[4] = 'nuovo'

# Rimuove la parentesi quadra aperta  [
# Rimuove la parentesi quadra chiusa ]
# Rimuove le virgole e le sostituisce con 2 spazi
path_in = ['/run/media/gianluca/Archivio/Wasted Years.flac',
           '/run/media/gianluca/Archivio/Caught Somewhere In Time.flac']
file_list = str(path_in).replace('[','').replace(']','').replace(',','  ')

# Concatenazioni di liste:
L1 = [1,2,3,4,5, 'z','x','v']

# Questa concatenazione fa sì di visualizzare altri elementi in coda alla
# lista L1 ma non cambierà il suo contenuto.
L1 + ['nazione', 'regione','città','paese']

# Per far sì di cambiare anche il suo contenuto si dovrà fare una nuova
# assegnazione, ma non sarà più la stessa lista poichè cambierà anche il
# suo id, comunque questo è lo svolgimento:
L1 = L1 + ['nazione', 'regione','città','paese']

# Per mantenere lo stesso id, cioè mantenere la stessa lista si usa il metodo
# extend() il quale *estende* la lista con un'altra lista mantenendo l'id:
array.extend(['radice', 'tronco','rami','foglie'])

# Modifica la lista invertendo l'ordine dei suoi elementi:
array.reverse()

# Modifica la lista riordinando i suoi elementi:
array.sort()

# Estende la lista con un'altra lista:
array.extend(['a','b','c'])

# Copia il contenuto della lista su un'altra lista:
altra = lista1[:]
#----------------------------------------------------------------------------#

############## SPACCHETTAMENTO

# Spacchettamento: assegna gli elementi dell'indice alle variabili a b c :
a,b,c = [1,2,3]

# Assegna elementi indicizzati, anche di differenti liste, alle variabili.
d,e,f = lista1[3],lista1[4],lista1[2]

# Esiste un'altra forma di spacchettamento chiamato spacchettamento esteso
# (extended iterable unpacking) che si differenzia per la presenza di una
# etichetta contrassegnata da un asterisco (Python guida completa pag 146)
a*,b,c = 'Python'
# la variabile a* diventerà una lista
#----------------------------------------------------------------------------#

############## CANCELLARE ELEMENTI IN LISTA

# Cancella uno specifico elemento nella lista con la keyword del:
del lista1[4]

# Cancella l'elemento in lista con il metodo remove():
# WARNING Se sono presenti più elementi con lo stesso nome *Questo* metodo
# rimuoverà solo il primo elemento incontrato nella sequenza.
lista1.remove('mela')

# Cancella uno specifico elemento dalla lista e ritorna il valore rimosso:
lista1.pop(2)
#----------------------------------------------------------------------------#

############## CANCELLARE ELEMENTI DUPLICATI

# Elimino eventuali doppioni; Attenzione che gli elementi della lista non
# avranno più l'ordine di origine!
set(lista1)

# Una pratica che può essere apportata su più progetti è la seguente:
from collections import Counter
LISTA1 = ['uno','due','uno',3,4,5,'nome',3,4,5,'cognome','indirizzo']
for k,v in Counter(LISTA1).items():# controllo doppioni accidentali.
        if v>1:
            print ("Removing following duplicates: > '%s' >" % (k))
    set(LISTA1) # removal of any duplicates in the list

# UPDATE puoi usare i set i quali non possono contenere doppioni
#----------------------------------------------------------------------------#

############## CANCELLAZIONE TOTALE CONTENUTI E CANCELLAZIONE TOTALE LISTE

# Cancellazione di tutti gli elementi della lista.
# In questo modo la lista sarà ancora presente in memoria ma sarà vuota:
del lista1[:]

# Cancella totalmente una lista.
# WARNING la lista non sarà più presente
del lista1

# Cancellazione di tutti gli elementi della lista con il metodo clear
lista1.clear()
#----------------------------------------------------------------------------#

############## CONVERSIONI DI LISTE, TUPLE E STRINGHE

# I metodi split(), join() e la funzione strip().

array = ['LOL','STANDARD MODE', '-vcodec libx264', 'wav flac ogg mp3', 'mkv']
new_li = [] # creo lista vuota

# Il 3° elemento (partendo da 0) della lista array viene messo in li
# ['wav','flac','ogg','mp3'] e separato in elementi distinti partendo
# dagli spazi vuoti.
li = array[3].split()

# itero sui elementi in li:
for a in li:
    # modifico gli elementi dati e li rimetto su new_li:
    new_li.append("*.%s;" % (a))

# la lista diventa una stringa con spazi:
stringher = ' '.join(new_li)

# mi da la lunghezza totale della stringa
lengher = len(stringher)

# rimuovo l'ultimo carattera della stringa lengher: (;)
part1 = "%s" % stringher[:lengher - 1]

# rimuove tutti gli spazi bianchi anche multipli in tutta la lunghezza della stringa:
part2 = "".join(part1.split())

# le virgole di separazione creano una tupla:
sum_all = "Audio source (%s)|%s|" % (part1, part2), "All files (*.*)|*.*"

lista = list(sum_all) # converto la tupla in lista

wildcard = ''.join(lista) # qui la lista diventa stringa ma senza spazi

# La funzione strip ha il compito di togliere gli spazi all'inizio e alla fine di una stringa
a = "   sii gentile ma spietato  "
print a.strip()
#----------------------------------------------------------------------------#

############## ALTRI METODI DELLE LISTE

# Order list by first element of nested list (change s[index] to order
# by other indexes:

li = [(23, 'xm', 'am'), (20, 'ca', 'ba'), (15, 'an', 'cum', 'po')]

# To leaving untouched original list you have to use sorted() function:
new = sorted(li, key=lambda s: s[0])  # this create a new list


# change original list in-place using sort method. Sort is a specific method
# for list only (dir(list)). For other data structures as frozen you have to
# use sorted function.
item.sort(key=lambda s: s[0])






