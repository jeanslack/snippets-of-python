#!/usr/bin/env python
# -*- coding: utf-8 -*-


#-----------------------------------------------------------------------
# se i file hanno un maching sicuro, cioè possiedono le stesse occorrenze
# in termini di numero di items, come nel caso seguente: 3 liste con
# 3 elementi ciascuna, allora si può utilizzare zip:

path = ["/home/sweet","/home/bigmama","/home/brother"]
name = ["file_1","file_2","file_3"]
ext = ["mp3","py","pdf"]

for p, n, e in zip(path, name, ext):
    print p,n,e

# Se però una o più mancanze di items è vera, succede che non vengono
# considerate le occorrenze vuote. Prova a togliere qualche item da
# una delle liste.
#-----------------------------------------------------------------------

# ...Allora abbiamo bisogno di map:
for p, n, e, in map(None, path, name, ext):
    print "%s/%s.%s" % (p,n,e)

if qualche item is None:
    qualche item = ''

#  < https://www.safaribooksonline.com/library/view/python-cookbook/0596001673/ch01s15.html>
"""
Looping Through Multiple Lists
Credit: Andy McKay

Problem
You need to loop through every item of multiple lists.

Solution
There are basically three approaches. Say you have:
"""
#-------------------------------- 1)
a = ['a1', 'a2', 'a3']
b = ['b1', 'b2']
"""
Using the built-in function map, with a first argument of None, you can iterate on both lists in parallel:
"""
print "Map:"
for x, y in map(None, a, b):
    print x, y
# The loop runs three times. On the last iteration, `y` will be None.

# Using the built-in function zip also lets you iterate in parallel:
#------------------------------- 2)
print "Zip:"
for x, y in zip(a, b):
    print x, y
# The loop runs two times; the third iteration simply is not done.

#------------------------------- 3)
# A list comprehension affords a very different iteration:

print "List comprehension:"
for x, y in [(x,y) for x in a for y in b]:
    print x, y
# The loop runs six times, over each item of b for each item of a.
"""
Discussion
Using map with None as the first argument is a subtle variation of the standard map call, which typically takes a function as the first argument. As the documentation indicates, if the first argument is None, the identity function is used as the function through which the arguments are mapped. If there are multiple list arguments, map returns a list consisting of tuples that contain the corresponding items from all lists (in other words, it’s a kind of transpose operation). The list arguments may be any kind of sequence, and the result is always a list.

Note that the first technique returns None for sequences in which there are no more elements. Therefore, the output of the first loop is:

Map:
a1 b1
a2 b2
a3 None

zip lets you iterate over the lists in a similar way, but only up to the number of elements of the smallest list. Therefore, the output of the second technique is:

Zip:
a1 b1
a2 b2
Python 2.0 introduced list comprehensions, with a syntax that some found a bit strange:

[(x,y) for x in a for y in b]
This iterates over list b for every element in a. These elements are put into a tuple (x, y). We then iterate through the resulting list of tuples in the outermost for loop. The output of the third technique, therefore, is quite different:

List comprehension:
a1 b1
a1 b2
a2 b1
a2 b2
a3 b1
a3 b2
See Also
The Library Reference section on sequence types; documentation for the zip and map built-ins in the Library Reference.
"""
#########   PYTHON 3   #############

# Su python 3 c'è una funzione che fa parte del modulo itertools che
# fa la stessa cosa di map() con maggiore efficienza
import itertools

for (x,y) in itertools.zip_longest(a,b):
    print ('%s %s' % (x,y))

