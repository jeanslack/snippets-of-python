#!/usr/bin/env python3

# Vari modi per creare un diz:
a = dict(one=1, two=2, three=3)
b = {'one': 1, 'two': 2, 'three': 3}
c = dict(zip(['one', 'two', 'three'], [1, 2, 3]))
d = dict([('two', 2), ('one', 1), ('three', 3)])
e = dict({'three': 3, 'one': 1, 'two': 2})

#----------------------------------------#

diz = {'abisso': 13, 'bacino': 11, 'cena': 12, 'delfino': 10, 'pozzo': 34}
# Per ottenere le chiavi secondo un ordine alfa-numerico dei valori.
# Use sorted with the get method as a key 
# (dictionary keys can be accessed by iterating):

sorted(diz, key=diz.get)

# ---------------------------------------#


# iter(dictview):(https://docs.python.org/3/library/stdtypes.html#dict)
#    Return an iterator over the keys, values or items (represented as 
#    tuples of (key, value)) in the dictionary.
#
#    Keys and values are iterated over in insertion order. This allows the 
#    creation of (value, key) pairs using zip(): 

pairs = zip(diz.values(), diz.keys()) # è un iteratore

#    Another way to create the same list is 
    
pairs = [(v, k) for (k, v) in diz.items()] # non è un iteratore

#    Iterating views while adding or deleting entries in the dictionary 
#    may raise a RuntimeError or fail to iterate over all entries.

#    Changed in version 3.7: Dictionary order is guaranteed to be insertion order.

## per vedere se un oggetto è un iteratore o meno
iter(pairs) is pairs
## deve dare True se iteratore, False altrimenti.

## Oppure:
from collections.abc import Iterable, Iterator
isinstance(pairs, Iterable)
isinstance(pairs, Iterator)

# ---------------------------------------#

# listare per keys o values:
# diz.keys(), diz.values() e diz.items() se instanziati con la classe list()
# danno una lista. Altrimenti si ottiene dict_..([..])

list(diz.keys())
['abisso', 'bacino', 'cena', 'delfino', 'pozzo', 'araldo', 'masha', 'orso']

diz.keys()
dict_keys(['abisso', 'bacino', 'cena', 'delfino', 'pozzo', 'araldo', 'masha', 'orso'])

