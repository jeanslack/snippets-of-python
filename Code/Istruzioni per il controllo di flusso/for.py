#!/usr/bin/python
# -*- coding: utf-8 -*-
def linea_sep():
  print"*******************************************"
linea_sep()

for a in range(1, 5):
	print "for con range annidato ",a
else:
	print 'The for loop is over'



# Questo programma stampa una sequenza di numeri, sequenza generata dalla funzione buil-in range.

# Per il funzionamento occorre fornire al programma due numeri e range restituisce una sequenza di numeri partendo dal primo numero inserito fino ad arrivare al secondo numero. Per esempio, range(1,5) restituisce la sequenza [1, 2, 3, 4]. Di default range effettua il conteggio con passo di 1. Se si specifica anche il terzo argomento (opzionale), questo diventa il passo. Ad esempio, range(1,5,2) restituisce [1,3]. Si ricordi che il secondo numero non viene mai incluso nella sequenza.

# Il ciclo for itera quindi su questo range specificato. for i in range(1,5) equivale a for i in [1, 2, 3, 4], ovvero assegnare ogni numero (o oggetto) della sequenza ad i, uno alla volta, e quindi eseguire il blocco di istruzioni per ogni valore di i. Nell'esempio specifico nel blocco di istruzioni, si stampa solo il suo valore.

# Ricordarsi che la parte else è opzionale. Quando viene inclusa, viene sempre eseguita una volta che il ciclo for è terminato a meno che non venga incontrata un'istruzione break.

# Ricordarsi anche che il ciclo for..in funziona per ogni tipo di sequenza. Nell'esempio veniva usata una lista di numeri generati dalla funzione built-in range, ma in generale si può usare un qualsiasi tipo di sequenza di oggetti. Si esplorerà questo concetto in dettaglio nei prossimi capitoli.

# ALTRI ESEMPI:
linea_sep()

lista1 = range(4) # la funzione range è stata assegnato come valore alla variabile, questa volta.
for b in lista1:
   print "1) for listato con range " + str(b) # Notare che la lista ottenuta con range è una lista di numeri, e non di stringhe, questo è il motivo dell'utilizzo della funzione "str" per convertire il numero in stringa da stampare a video.

linea_sep()

lista2 = range(1,4) # Se non si vuole cominciare da 0 basta indicare alla funzione range anche il valore iniziale.
for c in lista2:
   print "2) for listato con range 1,4 " + str(c)
