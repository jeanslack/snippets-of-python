#!/usr/bin/python
# -*- coding: utf-8 -*-


print """\n             ARGOMENTI TRATTATI IN QUESTO SCRIPT: 

    - if, else 
 
    - Operatori di confronto  
    < : minore  
    > : maggiore
    <= : è uguale o minore 
    >= : è uguale o maggiore 
    <> :

    - Operatori logici:
    and , & :        aggiunge e
    or , ^ , | :     aggiunge o  
    not , !  , <> :  aggiunge non
    == :             è uguale 

"""
 
anni=input("\nImmetti la tua età in formato numerico: ") # varibile tipo numerica

if 36 == anni:
	print "\nil 31 gennaio compi 37 anni!"

elif anni > 36 and anni <= 40:
	print "\nsei un adulto medio, hai oltre 36 anni ma non più di 40"

elif anni == 35 or anni >= 20 and anni < 36:
	print "\nsei giovane, hai meno di 36 anni, ma non meno di 20"

elif not anni > 20:
	print "\nnon raccontarmi palle!"

else: 
	print "\nNon ci credo, non sei così vecchio!!"
print 'Done'

mese=raw_input("\nScrivi il tuo mese di nascita: ")

if mese == "gennaio":
	print "     OK, Il 31 gennaio vero?"
elif mese != "gennaio":
	print "     Non è il tuo mese di nascita"
print 'Done'



