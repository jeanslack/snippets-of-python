#!/usr/bin/python
# -*- coding: utf-8 -*-

number = 23
running = True

while running:
    guess = int(raw_input('Enter an integer : '))

    if guess == number:
        print 'Congratulations, you guessed it.'
        running = False # questo determina l'uscita dal loop
    elif guess < number:
        print 'No, it is a little higher than that.'
    else:
        print 'No, it is a little lower than that.'
else:
    print 'The while loop is over.'
    # Aggiungere altro codice a partire da questo punto

print 'Done'

# In questo programma si gioca ancora ad indovinare un numero, ma il vantaggio ora è che all'utente si concede di giocare finché non indovina il numero corretto, e non c'è bisogno di eseguire nuovamente il programma per tentare di indovinare un nuovo numero. Questo è un modo adatto per dimostrare l'uso dell'istruzione while.

# Le istruzioni raw_input e if sono state poste all'interno di while è stata quindi assegnato il valore True alla variabile running prima del ciclo while. Si controlla se la variabile running è True e quindi si procede all'esecuzione del corrispondente blocco while. Dopo che questo blocco è stato eseguito, viene controllata nuovamente la condizione della variabile running. Se la condizione è ancora vera, viene eseguito di nuovo il blocco while, altrimenti viene eseguito il blocco else opzionale e quindi l'istruzione che segue.

# Il blocco else viene eseguito quando la condizione del ciclo while diventa False- Ciò può accadere anche quando la condizione viene controllata per la prima volta. Se c'è una clausola else per un ciclo while, questa viene sempre eseguita a meno che il ciclo while non contenga istruzioni di uscita.

# True e False vengono chiamati tipi Booleani e si possono considerare l'equivalente di 1 e 0 rispettivamente. E' importante usarli dove la condizione o il controllo (e non il loro contenuto numerico) sono importanti.

# Il blocco else è ridondante dato che le stesse istruzioni si possono collocare nel blocco seguente l'istruzione while ed ottenere lo stesso effetto. 

#-----------------------------------------------------

a = 0   
while a < 10:  
    a += 1 # assegnamento combinato: è lo stesso di a = a + 1
    print a
print 'done'

#-----------------------------------------------------

vero = True
ferma = 'no'

while vero:
    PC = raw_input('Vuoi farmi una domanda?')
    if PC == 'si':
        print 'Bene, sono tutto orecchi...'
    if PC == 'no':
        print 'Ok, pazienza!'
        vero = False 
print 'done'
