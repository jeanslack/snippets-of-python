#!/usr/bin/python
# -*- coding: utf-8 -*-


#  L'istruzione break viene usata per interrompere  un'istruzione ciclica, come ad esempio fermare l'esecuzione di un ciclo anche se la condizione di loop non ha ancora raggiunto il valore False o la sequenza di elementi deve essere ancora iterata.

# Un particolare degno di nota è che quando si interrompe un ciclo for o while, qualsiasi corrispondente blocco else non verrà mai eseguito

while True:
	s = raw_input('Enter something : ')
	if s == 'quit':
		break
	print 'Length of the string is', len(s)
print 'Done'


# In questo programma viene chiesto ripetutamente un input all'utente e viene poi stampata la lunghezza di ogni input. E' stata posta anche una condizione speciale per fermare il programma mediante il controllo sull'input per intercettare la sequenza di tasti 'quit'. Il programma viene fermato interrompendo il ciclo e raggiungendo così la fine del programma.

# La lunghezza della stringa di input può essere valutata con l'istruzione built-in len.

# Da ricordare che l'istruzione break può essere usata anche in un ciclo costruito con for. 
