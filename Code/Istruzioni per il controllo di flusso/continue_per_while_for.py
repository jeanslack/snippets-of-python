#!/usr/bin/python
# -*- coding: utf-8 -*-

# L'istruzione continue viene usata per saltare il resto delle istruzioni appartenenti al blocco di loop corrente e continuare  con la prossima iterazione del ciclo


while True:
	s = raw_input('Enter something : ')
	if s == 'quit':
		break
	if len(s) < 3:
		continue
	print 'Input is of sufficient length'
	# Il resto del codice a partire da questo punto...

#  In questo programma viene accettato ogni input fornito dall'utente, ma lo si processa solo se ha una lunghezza di almeno tre caratteri. Si usa la funzione built-in len per valutare la lunghezza dell'input e controllare che sia inferiore a tre, nel qual caso viene saltato il resto delle istruzione nel blocco con l'istruzione continue. Altrimenti viene eseguito il resto delle istruzioni del ciclo e si può fare qualsiasi tipo di elaborazione sia necessaria.

# L'istruzione continue può venire usata anche per cicli costruiti con for. 
