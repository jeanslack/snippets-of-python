#!/usr/bin/env python
# -*- coding: UTF-8 -*-

import re
import sys

class musicisti:
    """
    Definisco la classe exercise, una classe base che non eredita da
    nessun'altra classe (questo si nota dalla mancanza di parametri
    che in una definizione di classe equivarrebbe ad ereditariet�
    oppure a ereditariet� multipla se con molti parametri).

    """
    def __init__(self, List): # init sarebbe un costruttore
        """
        Tutte le funzioni dentro alle classi vengono chiamate metodi.
        Anche la funzione con init.
        Il primo argomento di ogni metodo di una classe, incluso __init__,
        � sempre un riferimento all'istanza corrente della classe.
        Per convenzione, questo argomento viene sempre chiamato self.
        Nel metodo __init__, self si riferisce all'oggetto appena creato;
        negli altri metodi della classe, si riferisce all'istanza da cui
        metodo � stato chiamato. Per quanto necessitiate di specificare
        self esplicitamente quando definite un metodo, non lo specificate
        quando chiamate il metodo; Python lo aggiunger� per voi automaticamente.
        """

        self.List = List

        datalines = []
        self.streams = []
        self.genere = []

        for a in self.List:

            if re.match('\[STREAM\]' ,a):
                datalines=[]

            elif re.match('\[\/STREAM\]' ,a):
                self.streams.append(analizza(datalines))
                #self.streams.append(datalines)
                datalines=[]

            else:
                datalines.append(a)

        #for a in self.streams:
            #if a.Genere():
                #self.genere.append(a)



class analizza:


    def __init__(self, datalines):
        diz = {'AUDIO' : ''}
        for a in datalines:
            (key, val) = a.strip().split(':')
            diz[key] = val

        print diz.items()




        #print self.__dict__

        #for a in datalines:
            #(key, val) = a.strip().split(':')
            #self.__dict__[key] = val



    #def Genere(self): # questo � un altro metodo
        ##audio = {'Audio' : ''}

        #if self.__dict__['MilesDavis']:
            #if str(self.__dict__['MilesDavis']) == 'jazz':





lista = ['[STREAM]', 'MilesDavis:jazz', 'OzzyOsbourne:metal', 'Genesis:Prog',
        'SoftMacchine:Canterbury', '[/STREAM],[STREAM]', 'JohnColtrane:jazz',
        'MilesDavis:jazz', 'Manovar:metal', 'KingCrimson:Prog',
        'Caravan:Canterbury', '[/STREAM], [STREAM]', 'MilesDavis:acidjazz',
        'Slayer:metal', 'PorcupineTree:Prog', 'IanCarr:Jazzrock', '[/STREAM]'
        ]
p = musicisti(lista) # questa � una istanza

p.List # � nel metodo init
