#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
questo è uno script
"""

class Student:
    """
    class student
    """
    def __init__(self, first_name, last_name):
        """ bla bla """
        self.first_name = first_name
        self.last_name = last_name

    def verify_registration_status(self):
        """ bla bla """
        status = self.get_status()
        self.status_verified = status == "registered"

    def get_guardian_name(self):
        """ bla bla """
        self.guardian = "Goodman"

    def get_status(self):
        """get the registration status from a database"""
        status = query_database(self.first_name, self.last_name)
        return status

    def __repr__(self):
        """ bla bla """
        return f"Student({self.first_name!r}, {self.last_name!r})"

    def __str__(self):
        """ bla bla """
        return f"Student: {self.first_name} {self.last_name}"

#op = Student('Mario', 'Cabassi')
##print(op.get_guardian_name())
#print(op)



class Goal:
    complete = False

    def set_complete_true(self):
        self.complete = True
        print(Goal.complete)

a = Goal()
a.complete = True
a.set_complete_true()
print(a.complete) # should be true now


