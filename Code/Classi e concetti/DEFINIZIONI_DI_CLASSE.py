#!/usr/bin/python
# -*- coding: UTF-8 -*-
#
#########################################################
# Name: DEFINIZIONI DI CLASSE (paradigma O.O.P.)
#
# Porpose: definizioni comuni tipiche sulla programmazione a oggetti
#          nel linguaggio formale di programmazione python, attraverso il
#          paradigma O.O.P, (object-oriented-programming).
#
# Writer: Gianluca Pernigoto <jeanlucperni@gmail.com>
# Copyright: (c) Gianluca Pernigoto <jeanlucperni@gmail.com>
# license: GPL3
# Version 0.0.1 - release Dec. 2017 -
# Rev
#########################################################
"""
Python supporta diversi paradigmi/stili di programmazione:

 procedurale
 funzionale
 a oggetti
 meta-programmazione
 scripting

In questo script si fa riferimento prevalentemente a una prospettiva
Object-Oriented.

Quella che segue è una Classe object, quella che viene definita Classe di base
nel linguaggio python. Object è la classe con il livello più alto perchè tutte
le altre classi ereditano da object. Essa è costituita, nella fattispecie, dalla
definizione di Classe stessa, dagli attributi di classe (o variabili di classe),
da un inizializzatore di istanza (dove vengono inizializzati gli attributi di
istanza, detti anche membri), da alcuni attributi e dai metodi della classe.

Per poter creare un'istanza alla classe, bisogna prima partire dal costruttore:

# IL COSTRUTTORE:
audio = People(argomento)

# E QUESTA È L'ISTANZA:
Formato = NomeCostruttore.Metodo(argomenti se richiesti o meno dai parametri
del metodo)

"""


class People(object):
    """
    Questa è la classe con il più altro livello; Qui vengono definite
    alcune variabili di classe (o attributi di classe) usando tipi
    diversi (None, int, list). Nota che le definizioni di tipo o classe
    sono interscambiabili perchè si tratta della stessa cosa.

    Se un attributo di classe viene impostato accedendo alla classe,
    sovrascriverà il valore per tutte le istanze.

    """
    class_var = 'DEFINED'
    class_mutable = []  # empty mutable type object
    class_varany = None

    def __init__(self, location):
        """
        __init__ è l'inizializzatore di istanza dove vengono inizializzati
        i membri (attributi di istanza). Il parametro `location` prende il
        valore dall'argomento passato sul costruttore.

        Quando vengono impostate le variabili di istanza il loro valore
        sarà presente solo per quella determinata istanza. Questo vale
        anche se una variabile di classe Python viene impostata accedendo
        a un'istanza, sovrascriverà il valore solo per quell'istanza.
        Questo essenzialmente sovrascrive la variabile di classe e la
        trasforma in una variabile di istanza disponibile, intuitivamente,
        solo per quell'istanza.
        """
        self.location = location
        self.river = None

    def method_1(self):
        """

        """
        pass
# ------------------------------------------------------------------------#

# <https://www.toptal.com/python/python-class-attributes-an-overly-thorough-guide>

# BEGIN CLASS ATTRIBUTES
'''
Per impostare una variabile di classe puoi usare in modo interscambiabile

    setattr(object, name, value)

oppure l'assegnamento

    object.name = value

'''
setattr(People, 'class_var', 34)  # setto class_var
# oppure:  People.class_var = 1  # setto class_var

setattr(People, 'otherclassvar', 'sono otherclassvar')
# oppure:  People.otherclassvar = 'sono otherclassvar'

print(People.class_var, People.otherclassvar)


# CONTROLLI UTILI PER LE VERIFICHE SULLE CLASSI
print(hasattr(People, 'otherclassvar'))  # check if has attribute

# ----------------------------------------------------------------- END

# BEGIN INSTANCE ATTRIBUTES

# COSTRUTTORI
prima = People('Cremona')
seconda = People('Bari')

'''
L'mpostazione di una variabile di istanza è analoga come in CLASS ATTRIBUTES
'''
setattr(prima, 'newvar', 'ciao')  # definisco nuova variabile di istanza
# oppure:  prima.newvar = 'ciao'  # definisco nuova variabile di istanza


# CONTROLLI UTILI PER LE VERIFICHE SULLE ISTANZE

if isinstance(prima, People):  # check if instance (prima) on class exists
    print(hasattr(prima, 'newvar'))  # then check if has newvar attribute

'''
Le classi Python e le istanze di classe hanno ciascuna i propri namespace distinti:
'''
print(People.__dict__)
print(prima.__dict__)


'''
When you try to access an attribute from an instance of a class, it first looks
at its instance namespace. If it finds the attribute, it returns the associated
value. If not, it then looks in the class namespace and returns the attribute
(if it’s present, throwing an error otherwise).
'''
print(seconda.location)  # Finds location in seconda's instance namespace
seconda.class_var  # Doesn't find class_var in instance namespace…
# So look's in class namespace (People.__dict__)

'''
Così, il namespace dell'istanza ha la supremazia sul namespace della classe:
se è presente un attributo con lo stesso nome in entrambi, lo spazio dei nomi
dell'istanza verrà controllato per primo e verrà restituito il suo valore.
'''

def instlookup(inst, name):
    ## simplified algorithm...
    if name in inst.__dict__:
    #if inst.__dict__.has_key(name):
        print("trovata nell'istanza")
        return inst.__dict__[name]
    else:
        print("trovata nella classe")
        return inst.__class__.__dict__[name]

ret = instlookup(seconda, 'class_var')



# ----------------------------------------------------------------- END

# BEGIN MUTABILITY

'''
Puoi manipolare L'attributo class accedendovi attraverso una particolare
istanza e, a sua volta, finire per manipolare l'oggetto referenziato a cui
accedono tutte le istanze.
'''

print(prima.class_var)  # > None
print(seconda.class_var)  # > None
prima.class_var = 'POPO'
seconda.class_var = 'OOOOOOO'
print(prima.class_var, seconda.class_var)  # > POPO
People.class_var = 'NOOOO'
print(People.class_var,
      prima.class_var,
      seconda.class_var)  # > NOOOO POPO OOOOOOO

'''
Ma se hai un oggetto mutabile (list) definito come attributo di classe
(class_mutable) otteniamo il seguente comportamento
'''
prima.class_mutable.append('mare')
seconda.class_mutable.append('cielo')

print(prima.class_mutable)   # ['mare', 'cielo']
print(seconda.class_mutable)  # ['mare', 'cielo']

'''
Potremmo aggirare questo problema usando l'assegnazione; ovvero, invece di
sfruttare la mutabilità della lista, potremmo assegnare ai nostri oggetti
People le proprie liste, come segue:
'''
prima.class_mutable = ['mare']
seconda.class_mutable = ['cielo']

print(prima.class_mutable)   # ['mare']
print(seconda.class_mutable)  # ['cielo']

'''
Questo comportamento non è molto comodo, soggetto a errori, dimenticanze, etc.
Una soluzione è quella di non utilizzare valori mutabili in default ([])
sugli attributi di classe e considerare di utilizzarli solo come attributi di
istanza. In alternativa assegna all'attributo di classe un valore None
come in class_varany.
'''
# ----------------------------------------------------------------- END

# BEGIN CONCLUSION

'''
Gli attributi di classe sono complicati, ma diamo un'occhiata ad alcuni casi in
cui potrebbero tornare utili:
'''

# 1. storing constants
PI = 3.14159

# 2. Defining default values
limit = 10

# 3. Tracking all data across all instances of a given class
prima.class_mutable.append('mare')
seconda.class_mutable.append('cielo')

print(prima.class_mutable)   # ['mare', 'cielo']
print(seconda.class_mutable)  # ['mare', 'cielo']

# 4. Performance



